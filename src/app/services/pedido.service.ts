import { Injectable } from '@angular/core';
import { Pedido } from '../pedido/pedido';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable,throwError  } from 'rxjs';
import { retry, catchError,map } from 'rxjs/operators';
import { BaseService } from './baseService';
@Injectable({
  providedIn: 'root'
})
export class PedidoService extends BaseService {

  public url = "http://localhost/sisvendasv3/"
  public local= "https://localhost:5001/"
  public pedido: Pedido[];
 
  constructor(private http: HttpClient) {super()


   }
 
   getClientelogin() {

    // return this.get<Usuario[]>(this.url + '?login=' + login + '&senha=' + senha);
    return this.http.get<Pedido[]>(this.url + 'api/Pedidos',super.ObterAuthHeaderJson()).subscribe(result => {
        this.pedido = result;
    }, error => console.error(error));
} 
createCliente(pedido: Pedido): Observable<Pedido> {
 
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  return this.http.post<any>(this.url + 'api/Pedidos',
      pedido,super.ObterAuthHeaderJson()).pipe(
      
        retry(1),
        catchError(this.errorHandl2)
      );
}

createPedidos(pedidos: any): Observable<any> {
 
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  return this.http.post<any[]>(this.url + 'cadastrartodos',
  pedidos, super.ObterAuthHeaderJson()).pipe(
      
        retry(1),
        catchError(this.errorHandl2)
      );
}
AlterarPedidos(pedidos: any): Observable<any> {
 
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  return this.http.put<any[]>(this.url + 'alterarpedido',
  pedidos, super.ObterAuthHeaderJson()).pipe(
      
        retry(1),
        catchError(this.errorHandl2)
      );
}

getpedidoitem(codigopedido: number): Observable<Pedido[]>    
  {    
    return this.http.get<Pedido[]>(this.url + 'api/pedidoItems/'+codigopedido,super.ObterAuthHeaderJson())
  }  
getPedidosList(): Observable<Pedido[]> {
  return this.http.get<Pedido[]>(this.url + 'api/Pedidos',super.ObterAuthHeaderJson())
  .pipe(
   map((data:Pedido[])=>data)
   
    
   // retry(1),
    //catchError(this.errorHandl2)
  );
}
getPedidosById(id: number) {
  return this.http.get<Pedido>(this.url + 'api/Pedidos/' + id,super.ObterAuthHeaderJson());
}
getPedidosResumo(id: number) {
  return this.http.get<any>(this.url + 'PedidoResumo/' + id,super.ObterAuthHeaderJson());
}
getPedidosProdutoResumo(id: number) {
  return this.http.get<any>(this.url + 'resultadoProdPed/' + id,super.ObterAuthHeaderJson());
}
// Error handling
errorHandl2(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  console.log(errorMessage);
  return throwError(errorMessage);
}
}
