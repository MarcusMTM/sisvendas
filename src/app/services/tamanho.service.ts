import { Injectable } from '@angular/core';
import { Tamanho } from '../tamanho/tamanho';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable,throwError  } from 'rxjs';
import { retry, catchError,map,tap } from 'rxjs/operators';
import { BaseService } from './baseService';
@Injectable({
  providedIn: 'root'
})
export class TamanhoService extends BaseService {

  public url = "http://localhost/sisvendasv3/"
  public tamanho: Tamanho[];
 
  constructor(private http: HttpClient) { super()


   }
 
   getTamanhos() {

  
    return this.http.get<Tamanho[]>(this.url + 'api/tamanhoes').subscribe(result => {
        this.tamanho = result;
    }, error => console.error(error));
} 
TamanhoList() {  
  this.http.get(this.url + 'api/tamanhoes',super.ObterAuthHeaderJson()).subscribe(result=>this.tamanho = result as Tamanho[])  
}
getTamanhoById(id: number) {
  return this.http.get<Tamanho>(this.url + 'api/tamanhoes/' + id,super.ObterAuthHeaderJson());
}
getTamanhoList(): Observable<Tamanho[]> {
  return this.http.get<Tamanho[]>(this.url + 'api/tamanhoes',super.ObterAuthHeaderJson())
  // .pipe(
  //  map((data:Tamanho[])=>data)
   .pipe(
    retry(1),
    catchError(this.serviceError));
    
   // retry(1),
    //catchError(this.errorHandl2)
  //);
}

getTamanhosInt(codigo:number ) {

  // return this.get<Usuario[]>(this.url + '?login=' + login + '&senha=' + senha);
  return this.http.get<Tamanho[]>(this.url + 'api/tamanhoes/'+codigo).subscribe(result => {
      this.tamanho = result;
  }, error => console.error(error));
} 
createTamanhos(tamanho: Tamanho): Observable<Tamanho> {
 
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  return this.http.post<Tamanho>(this.url + 'api/tamanhoes',
  tamanho, super.ObterAuthHeaderJson()).pipe(
        retry(1),
        catchError(this.errorHandl2)
      )
}
updateTamanho(tamanho: Tamanho) {
 
 
  return this.http.put<Tamanho[]>(this.url + 'api/tamanhoes/' + tamanho.codigotamanho, tamanho,super.ObterAuthHeaderJson()).pipe(
    retry(1),
    catchError(this.errorHandl2)
  )
 //return this.http.put(this.url + 'api/Produtos/' + produto.codigoProduto.toString(), produto,super.ObterAuthHeaderJson());
}
// Error handling
errorHandl2(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  console.log(errorMessage);
  return throwError(errorMessage);
}
}
