import { Injectable } from '@angular/core';
import { pedidoitem } from '../pedidoitem/pedidoItem';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable,throwError  } from 'rxjs';
import { retry, catchError,map } from 'rxjs/operators';
import { BaseService } from './baseService';
@Injectable({
  providedIn: 'root'
})
export class PedidoitemService  extends BaseService{


  public url = "http://localhost/sisvendasv3/"
  public pedidoitem: pedidoitem[];
 
  constructor(private http: HttpClient) {super()


   }
 
   getpedidoitem() {

    // return this.get<Usuario[]>(this.url + '?login=' + login + '&senha=' + senha);
    return this.http.get<pedidoitem[]>(this.url + 'api/pedidoItems',super.ObterAuthHeaderJson()).subscribe(result => {
        this.pedidoitem = result;
    }, error => console.error(error));
} 
createpedidoitem(pedidoitem: pedidoitem): Observable<pedidoitem> {
 
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  return this.http.post<pedidoitem>(this.url + 'api/pedidoItems',
  pedidoitem, httpOptions).pipe(
        retry(1),
        catchError(this.errorHandl2)
      )
}

getPedidosList(): Observable<pedidoitem[]> {
  return this.http.get<pedidoitem[]>(this.url + 'api/pedidoItems',super.ObterAuthHeaderJson())
  .pipe(
   map((data:pedidoitem[])=>data)
   
    
   // retry(1),
    //catchError(this.errorHandl2)
  );
}
PedidoItemResumo
getPedidosById(id: number) {
  return this.http.get<pedidoitem>(this.url + 'api/pedidoItems/' + id,super.ObterAuthHeaderJson());
}
getPedidoItemResumo(id: number) {
  return this.http.get<pedidoitem>(this.url + 'PedidoItemResumo/' + id,super.ObterAuthHeaderJson());
}
getPedidosId(id: number): Observable<pedidoitem[]> {
  return this.http.get<pedidoitem[]>(this.url + 'api/pedidoItems/' + id,super.ObterAuthHeaderJson())
  .pipe(
    map((data:pedidoitem[])=>data)
    
     
    // retry(1),
     //catchError(this.errorHandl2)
   );
}
// Error handling
errorHandl2(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  console.log(errorMessage);
  return throwError(errorMessage);
}


}
