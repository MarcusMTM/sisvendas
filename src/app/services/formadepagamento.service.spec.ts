import { TestBed } from '@angular/core/testing';

import { FormadepagamentoService } from './formadepagamento.service';

describe('FormadepagamentoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FormadepagamentoService = TestBed.get(FormadepagamentoService);
    expect(service).toBeTruthy();
  });
});
