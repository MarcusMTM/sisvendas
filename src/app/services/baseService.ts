import {  HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { throwError } from 'rxjs';

export abstract class BaseService {

    protected UrlServiceV1: string = "http://localhost/sisvendasV3/";
    //protected UrlServiceV1: string = "https://devioapi.azurewebsites.net/api/v1/";

    protected ObterHeaderFormData() {
        return {
            headers: new HttpHeaders({
                'Content-Disposition': 'form-data; ',
                'Authorization': `Bearer ${this.obterTokenUsuario()}`
            })
        };
    }

    protected ObterHeaderJson() {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
    }

    protected ObterAuthHeaderJson(){
     
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.obterTokenUsuario()}`
            })
        };
    }

    protected extractData(response: any) {
        
        return response || {};
    }

    public obterUsuario() {
        return JSON.parse(localStorage.getItem('userToken.nomeusuario'));
    }

    protected obterTokenUsuario(): string {
     
        return localStorage.getItem('accessToken');
    }

    // protected serviceError(error: Response | any) {
    //     let errMsg: string;

    //     if (error instanceof Response) {

    //         errMsg = `${error.status} - ${error.statusText || ''}`;
    //     }
    //     else {
    //         errMsg = error.message ? error.message : error.toString();
    //     }

        
    //     return throwError(errMsg);
    // }
    protected serviceError(response: Response | any) {
        let customError: string[] = [];

        if (response instanceof HttpErrorResponse) {

            if (response.statusText === "Unknown Error") {
                customError.push("Ocorreu um erro desconhecido");
                response.error.errors = customError;
            }
        }

        console.error(response);
        return throwError(response);
    }
}