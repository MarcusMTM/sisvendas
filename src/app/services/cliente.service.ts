import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable,throwError  } from 'rxjs';
import { retry, catchError,map } from 'rxjs/operators';
import { Cliente } from '../cliente/cliente';
import { BaseService } from './baseService';
@Injectable({
  providedIn: 'root'
})
export class ClienteService extends BaseService {

  public url = "http://localhost/sisvendasv3/"
  public cliente: Cliente[];
 
  constructor(private http: HttpClient) {super()


   }
 
   getClientelogin() {

    // return this.get<Usuario[]>(this.url + '?login=' + login + '&senha=' + senha);
    return this.http.get<Cliente[]>(this.url + 'api/clientes').subscribe(result => {
        this.cliente = result;
    }, error => console.error(error));
} 
createCliente(usu: Cliente): Observable<Cliente> {
 
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  return this.http.post<Cliente>(this.url + 'api/clientes',
      usu,super.ObterAuthHeaderJson()).pipe(
        retry(1),
        catchError(super.serviceError)
      )
}
getClienteById(id: number) {
  return this.http.get<Cliente>(this.url + 'api/clientes/' + id,super.ObterAuthHeaderJson());
}


updateCliente(cliente: Cliente) 
{
  return this.http.put<Cliente[]>(this.url + 'api/clientes/' + cliente.codigoCliente, cliente,super.ObterAuthHeaderJson()).pipe(
    retry(1),
    catchError(this.errorHandl2)
  )
 //return this.http.put(this.url + 'api/Produtos/' + produto.codigoProduto.toString(), produto,super.ObterAuthHeaderJson());
}
getClienteList(): Observable<Cliente[]> {
  return this.http.get<Cliente[]>(this.url + 'api/clientes',super.ObterAuthHeaderJson())
  .pipe(
   map((data:Cliente[])=>data)

  );
}
// Error handling
errorHandl2(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  console.log(errorMessage);
  return throwError(errorMessage);
}
}
