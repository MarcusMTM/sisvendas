import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable,throwError  } from 'rxjs';
import { retry, catchError,map,tap } from 'rxjs/operators';
import { formapagamento } from '../pedido/formapagamento';
import { BaseService } from './baseService';
@Injectable({
  providedIn: 'root'
})
export class FormadepagamentoService   extends BaseService{

  public url = "http://localhost/sisvendasv3/"
  public formapagamentos: formapagamento[];
 
  constructor(private http: HttpClient) {super()


   }
   getColecao() {

    // return this.get<Usuario[]>(this.url + '?login=' + login + '&senha=' + senha);
    return this.http.get<formapagamento[]>(this.url + 'api/formapagamentoes',super.ObterAuthHeaderJson()).subscribe(result => {
        this.formapagamentos = result;
    }, error => console.error(error));
} 
createColecao(formapagamento: formapagamento): Observable<formapagamento> {
 
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  return this.http.post<formapagamento>(this.url + '/api/formapagamentoes',
  formapagamento,super.ObterAuthHeaderJson()).pipe(
        retry(1),
        catchError(this.errorHandl2)
      )
}

getformapagamentoList(): Observable<formapagamento[]> {
  return this.http.get<formapagamento[]>(this.url + 'api/formapagamentoes',super.ObterAuthHeaderJson())
  .pipe(
   map((data:formapagamento[])=>data)

  );
}
updateformapagamento(formapagamento: formapagamento) {
 
  return this.http.put<formapagamento[]>('http://localhost/sisvendasv3/api/Usuarios/' + formapagamento.codigoFormaPagamento, formapagamento,super.ObterAuthHeaderJson()).pipe(
    retry(1),
    catchError(this.errorHandl2)
  )
 //return this.http.put(this.url + 'api/Produtos/' + produto.codigoProduto.toString(), produto,super.ObterAuthHeaderJson());
}
// Error handling
errorHandl2(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  console.log(errorMessage);
  return throwError(errorMessage);
}
}