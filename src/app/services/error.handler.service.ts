import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";

import { LocalStorageUtils } from '../utils/LocalStorageUtils';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor(private router: Router,  private toastr: ToastrService) { }

    localStorageUtil = new LocalStorageUtils();

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(req).pipe(catchError(error => {

            if (error instanceof HttpErrorResponse) {
                    
                if (error.status === 401) {
                    this.localStorageUtil.limparDadosLocaisUsuario();
                    this.toastr.error('Sessão expirou!', 'Opa :(');
                    this.router.navigate(['/entrar']);
                }
                if (error.status === 403) {
                    this.router.navigate(['/acesso-negado']);
                }
                if (error.status === 404) {
                    console.log('teste 404')
                    this.toastr.error('erro', error.message);
                    this.router.navigate(['/entrar']);
                }
            }

            return throwError(error);
        }));
    }

}