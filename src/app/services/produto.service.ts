import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders} from '@angular/common/http';

import { Observable,throwError  } from 'rxjs';
import { retry, catchError,map } from 'rxjs/operators';
import { Produtos } from '../produto/Produtos'
import {ProdutosSelecionado} from '../pedido/ProdutoSelecionado'
import { BaseService } from './baseService';
@Injectable({
  providedIn: 'root'
})
export class ProdutoService extends BaseService{

  public url = "http://localhost/sisvendasv3/"
  //'http://localhost/sisvendasv2/api/produtos';
  public Produtos: Produtos[];
 
  constructor(private http: HttpClient) { super()


   }
 
   getProdutos() {

    // return this.get<Usuario[]>(this.url + '?login=' + login + '&senha=' + senha);
    return this.http.get<Produtos[]>(this.url + 'api/Produtos',super.ObterAuthHeaderJson()).subscribe(result => {
        this.Produtos = result;
    }, error => console.error(error));
} 
getProduto() {

  // return this.get<Usuario[]>(this.url + '?login=' + login + '&senha=' + senha);
  return this.http.get<Produtos[]>(this.url + 'api/Produtos',super.ObterAuthHeaderJson()).subscribe(result => {
      this.Produtos = result;
  }, error => console.error(error));
} 
createCliente(produto: Produtos): Observable<Produtos> {
 
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' ,'Authorization': `Bearer ${this.obterTokenUsuario()}`}) };
  return this.http.post<Produtos>(this.url + 'api/Produtos',
  produto,super.ObterAuthHeaderJson()).pipe(
        retry(1),
        catchError(this.errorHandl2)
      )
}

getProdutosList(): Observable<any[]> {
  return this.http.get<any[]>(this.url + 'api/Produtos',super.ObterAuthHeaderJson())
  .pipe(
   map((data:any[])=>data)

  );
}
getProdutosById(id: number) {
  return this.http.get<Produtos>(this.url + 'api/Produtos/' + id,super.ObterAuthHeaderJson());
}
getProdutoSelecionado(): Observable<any[]> {
  return this.http.get<ProdutosSelecionado[]>(this.url + 'ProdutoSelecionado',super.ObterAuthHeaderJson())
  .pipe(
   map((data:ProdutosSelecionado[])=>data)

  );
}

updateProduto(produto: Produtos) {
  const url = `${this.url + 'api/Produtos'}/${produto.codigoProduto}`;
  var headerOptions = new Headers({ 'Content-Type': 'application/json' });
   // var requestOptions = new RequestOptions({ method: RequestMethod.Put, headers: headerOptions });
  console.log('produto'+url+produto.codigoProduto.toString())
  return this.http.put<Produtos[]>('http://localhost/sisvendasv3/api/Produtos/' + produto.codigoProduto, produto,super.ObterAuthHeaderJson()).pipe(
    retry(1),
    catchError(this.errorHandl2)
  )
 //return this.http.put(this.url + 'api/Produtos/' + produto.codigoProduto.toString(), produto,super.ObterAuthHeaderJson());
}

deleteUser(id: number) {
  console.log('id'+id)
  return this.http.delete<Produtos[]>(this.url+"api/Produtos/" + id,super.ObterAuthHeaderJson());  
  
}
update(id, data) {
  return this.http.put(`${this.url+"api/Produtos"}/${id}`, data,super.ObterAuthHeaderJson());
}
// Error handling
errorHandl2(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  console.log(errorMessage);
  return throwError(errorMessage);
}
}
