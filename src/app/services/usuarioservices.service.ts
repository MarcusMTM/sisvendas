import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { retry, catchError,map,tap } from 'rxjs/operators';
import { Usuario } from '../usuarios/usuario';
import { BaseService } from './baseService';

@Injectable({
  providedIn: 'root'
})
export class UsuarioservicesService   extends BaseService {
  public url = "http://localhost/sisvendasv3/"
  public usuarios: Usuario[];
 
  constructor(private http: HttpClient) {super() }

   getUsuariologin() {

    // return this.get<Usuario[]>(this.url + '?login=' + login + '&senha=' + senha);
    return this.http.get<Usuario[]>(this.url + 'api/Usuarios').subscribe(result => {
        this.usuarios = result;
    }, error => console.error(error));
} 
getlogin(usu: Usuario) {
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  return this.http
  .post("http://localhost/sisvendasV3/login", usu,super.ObterHeaderJson())
    .pipe(
     
      map(super.extractData),
      catchError(super.serviceError)
        );
       
} 
persistirUserApp(response: any){
  console.log(JSON.stringify(response))
  localStorage.setItem('accessToken', response.accessToken);
  localStorage.setItem('userToken.nomeusuario', JSON.stringify(response.userToken.nomeusuario));
}
getUsuarionome(Nome: string): Observable<Usuario[]>    
  {    
    return this.http.get<Usuario[]>(this.url + 'api/Usuarios/GetUsuarioNome/'+Nome)
  }  

createUsuario(usu: Usuario): Observable<Usuario> {
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  return this.http.post<Usuario>(this.url + 'api/Usuarios',
      usu,super.ObterAuthHeaderJson());
}
getUsuarioById(id: number) {
  return this.http.get<Usuario>(this.url + 'api/Usuarios/' + id,super.ObterAuthHeaderJson());
}
updateUsuario(usu: Usuario) {
 
  return this.http.put<Usuario[]>('http://localhost/sisvendasv3/api/Usuarios/' + usu.codigoUsuario, usu,super.ObterAuthHeaderJson()).pipe(
    retry(1),
    catchError(this.errorHandl)
  )
 //return this.http.put(this.url + 'api/Produtos/' + produto.codigoProduto.toString(), produto,super.ObterAuthHeaderJson());
}
// Error handling
errorHandl(error) {
  let errorMessage = '';
  if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
  } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  console.log(errorMessage);
  return (errorMessage);
}
getUsusarioList(): Observable<Usuario[]> {
  return this.http.get<Usuario[]>(this.url + 'api/Usuarios',super.ObterAuthHeaderJson())
  .pipe(
   map((data:Usuario[])=>data),
   catchError(super.serviceError)
     );
}
}

