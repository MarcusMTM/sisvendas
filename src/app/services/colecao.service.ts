import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable,throwError  } from 'rxjs';
import { retry, catchError,map,tap } from 'rxjs/operators';
import { Colecao } from '../Colecao/colecao';
import { BaseService } from './baseService';

@Injectable({
  providedIn: 'root'
})
export class ColecaoService extends BaseService{

  public url = "http://localhost/sisvendasv3/"
  public colecao: Colecao[];
 
  constructor(private http: HttpClient) {super()


   }
 
   getColecao() {

    // return this.get<Usuario[]>(this.url + '?login=' + login + '&senha=' + senha);
    return this.http.get<Colecao[]>(this.url + 'api/Colecaos',super.ObterAuthHeaderJson()).subscribe(result => {
        this.colecao = result;
    }, error => console.error(error));
} 
createColecao(usu: Colecao): Observable<Colecao> {
 
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  return this.http.post<Colecao>(this.url + 'api/Colecaos',
      usu,super.ObterAuthHeaderJson()).pipe(
        retry(1),
        catchError(this.errorHandl2)
      )
}
getColecaoList(): Observable<Colecao[]> {
  return this.http.get<Colecao[]>(this.url + 'api/Colecaos',super.ObterAuthHeaderJson())
  .pipe(
   map((data:Colecao[])=>data)
   
    
   // retry(1),
    ,catchError(this.errorHandl2)
  );
}
getUserById(id: number) {
  return this.http.get<Colecao>(this.url + 'api/Colecaos/' + id,super.ObterAuthHeaderJson());
}

createUser(colecao: Colecao) {
  return this.http.post(this.url, colecao);
}


updateColecao(colecao: Colecao) {


  return this.http.put<Colecao[]>('http://localhost/sisvendasv3/api/Colecaos/' + colecao.codigoColecao, colecao,super.ObterAuthHeaderJson()).pipe(
    retry(1),
    catchError(this.errorHandl2)
  )
 //return this.http.put(this.url + 'api/Produtos/' + produto.codigoProduto.toString(), produto,super.ObterAuthHeaderJson());
}

deleteUser(id: number) {
  console.log('id'+id)
  return this.http.delete<Colecao[]>(this.url+"api/Colecaos/" + id,super.ObterAuthHeaderJson());  
  
}
// Error handling
errorHandl2(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  console.log(errorMessage);
  return throwError(errorMessage);
}
}
