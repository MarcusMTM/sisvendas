import { TestBed } from '@angular/core/testing';

import { PedidoitemService } from './pedidoitem.service';

describe('PedidoitemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PedidoitemService = TestBed.get(PedidoitemService);
    expect(service).toBeTruthy();
  });
});
