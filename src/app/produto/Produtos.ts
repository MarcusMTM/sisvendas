import { Tamanho } from '../tamanho/tamanho';
import { Colecao } from '../colecao/colecao';

export class Produtos {
    
    codigoProduto: number;
	//nome: string;
	descricao: string ;
	codigoTamanho :number;
	codigoColecao :number;
    valor :number  ;
	status:boolean;
	constructor(codigoTamanho:Tamanho,descricao:string,codigoColecao:Colecao,descricaocolecao:string){
		codigoTamanho = codigoTamanho;
		codigoColecao=codigoColecao;
		descricao=descricao;
		descricaocolecao=descricao;
	 }
	
}

