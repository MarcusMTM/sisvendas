import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EditProdutoComponent } from './edit-produto/edit-produto.component';

import { ProdutosAppComponent } from './produtos.app.component';


const podutoRouterConfig: Routes = [
    {
        path: '', component: ProdutosAppComponent,
        children: [
            { path: 'editProduto', component: EditProdutoComponent}
         
            //{ path: 'cadastro', component: CadastroComponent, canActivate: [ContaGuard], canDeactivate: [ContaGuard] },
           // { path: 'login', component: LoginComponent, canActivate: [ContaGuard] }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(podutoRouterConfig)
    ],
    exports: [RouterModule]
})
export class ProdutoRoutingModule { }