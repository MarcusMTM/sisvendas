import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { ProdutoRoutingModule } from './produto.route';
import { ProdutosAppComponent } from './produtos.app.component';



@NgModule({
  declarations: [
   
    ProdutosAppComponent
  ]
  ,
  imports: [
    CommonModule,
    RouterModule,
    ProdutoRoutingModule
  ],
  exports:[
    
  ]
})
export class ProdutoModule { }
