import { Component, OnInit, NgZone  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProdutoService } from '../../services/produto.service';
import { TamanhoService } from '../../services/tamanho.service';
import { ColecaoService } from '../../services/colecao.service';
import { Tamanho } from '../../tamanho/tamanho';
import { Observable } from 'rxjs';
import { Colecao } from 'src/app/colecao/colecao';
import { ToastrService } from 'ngx-toastr';
import { MASKS, NgBrazilValidators } from 'ng-brazil';
import { StringUtils } from 'src/app/utils/string-utils';
import { NgxSpinnerService } from "ngx-spinner";


import { Produtos } from '../Produtos';
@Component({
  selector: 'app-create-produto',
  templateUrl: './create-produto.component.html',
  styleUrls: ['./create-produto.component.scss']
})
export class CreateProdutoComponent implements OnInit {
  produto: Produtos;
  ProdutoForm: FormGroup;
  IssueArr: any = [];
  submitted = false;
  ListTamanhos:Tamanho[];
  tamanhos$: Observable<Tamanho[]>;
  colecao$: Observable<Colecao[]>;
  errors: any[] = [];
  //date: {year: number, month: number};
  mudancasNaoSalvas: boolean;
  public MASKS = MASKS;
  constructor(
      public fb: FormBuilder,
      private ngZone: NgZone,
      private router: Router,
      public produtoService: ProdutoService,
      public tamanhoService:TamanhoService,
      public colecaoService:ColecaoService,
      private toastr: ToastrService,
      private spinner: NgxSpinnerService
      //public errorService: ErrorService
    ) {}

  ngOnInit() {
    

  

      this.addIssue()
     // this.tamanhoService.getTamanhos(); 
      this.gettamanho();
      this.getColecao();
     
      //this.listtamanho()
}
/* Conver date object to string */
getFormattedPrice(price: number) {
  return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(price);
}
  addIssue() {
      this.ProdutoForm = this.fb.group({
      
        descricao: ['',Validators.required],
        codigoTamanho: ['',Validators.required],
        codigocolecao: ['',Validators.required],
        valor: ['', [<any>Validators.required, <any>NgBrazilValidators.currency]],
      })
        
      
  }
  submitForm() {
    this.submitted = true;
    if (this.ProdutoForm.invalid) {
      alert('ERR0!! :-)\n\n' + JSON.stringify(this.ProdutoForm.value))
        return;
    }else{
    let valorproduto=this.ProdutoForm.controls['valor'].value
    console.log(valorproduto)
    //const  valorteste= StringUtils.somenteNumeros(valorproduto)
    
    this.produto = Object.assign({}, this.produto, this.ProdutoForm.value);
    this.spinner.show();
    this.produto.valor= (valorproduto);
      this.produtoService.createCliente(this.produto).subscribe(  
        sucesso => { this.processarSucesso(sucesso) },
        falha => { this.processarFalha(falha) }
         
         // this.ngZone.run(() => this.router.navigateByUrl('/ListProduto'))
      );
    }
  }
  BindTamanho(Tamanhocodigo : number){  
    console.log('teste tamanho'+Tamanhocodigo)
 this.tamanhoService.getTamanhosInt(Tamanhocodigo);  
 }  

 gettamanho(){
   
  this.tamanhos$= this.tamanhoService.getTamanhoList();
  console.log('teste') 
  console.log( this.tamanhos$)
 }
 getColecao(){
   
  this.colecao$= this.colecaoService.getColecaoList();
  console.log('teste colecao') 
 
 }
 voltar(): void {
  localStorage.removeItem("editcodigoProduto");
 

  this.router.navigate(['ListProduto']);  
};
processarFalha(fail: any) {
  this.errors = fail.error.errors;
  this.toastr.error('Ocorreu um erro!', 'Opa :(');
}
processarSucesso(response: any) {
  this.errors = [];    
  this.mudancasNaoSalvas = false;
  
  let toast = this.toastr.success('cadastrado com sucesso!', 'Sucesso!');
  if (toast) {
    toast.onHidden.subscribe(() => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
     
      this.ProdutoForm.reset();
      this.router.navigate(['ListProduto']);
      }, 1000);
    });
  }
 
  
}

}
