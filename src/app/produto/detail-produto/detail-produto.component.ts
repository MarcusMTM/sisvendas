import { Component, OnInit ,NgZone  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProdutoService } from '../../services/produto.service';
import { TamanhoService } from '../../services/tamanho.service';
import { ColecaoService } from '../../services/colecao.service';
import { Tamanho } from '../../tamanho/tamanho';
import { Observable } from 'rxjs';
import { Colecao } from 'src/app/colecao/colecao';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-detail-produto',
  templateUrl: './detail-produto.component.html',
  styleUrls: ['./detail-produto.component.scss']
})
export class DetailProdutoComponent implements OnInit {

  ProdutoForm: FormGroup;
  IssueArr: any = [];
  submitted = false;
  ListTamanhos:Tamanho[];
  tamanhos$: Observable<Tamanho[]>;
  colecao$: Observable<Colecao[]>;
 
  constructor(
      public fb: FormBuilder,
      private ngZone: NgZone,
      private router: Router,
      public produtoService: ProdutoService,
      public tamanhoService:TamanhoService,
      public colecaoService:ColecaoService,
      private modalService: NgbModal
      //public errorService: ErrorService
    ) {}

  ngOnInit() {
    this.gettamanho();
    this.getColecao();
    console.log('Issue editcodigoProduto!')
      this.addIssue()
      let empid = localStorage.getItem('editcodigoProduto');  
      if (+empid > 0) {  
        this.produtoService.getProdutosById(+empid).subscribe(data => {  
          //  this.ColecaoForm.setValue({
          //   dataValidade:formatDate(data.dataValidade.toDateString(), 'yyyy-MM-dd', 'en-US'),
          //    descricao:data.descricao.toString()});
          console.log(data)
          this.ProdutoForm.patchValue(data); 
          
         // this.ColecaoForm.patchValue(data)
        })  
        // this.btnvisibility = false;  
        // this.empformlabel = 'Edit Employee';  
        // this.empformbtn = 'Update';  
      } 
    
      //this.listtamanho()
}
/* Conver date object to string */

  addIssue() {
      this.ProdutoForm = this.fb.group({
      
        descricao: [{value: '', disabled: true},Validators.required],
        codigoTamanho: [{value: '', disabled: true},Validators.required],
        codigoColecao: [{value: '', disabled: true},Validators.required],
        valor: [{value: '', disabled: true},Validators.required],
      })
        
      
  }
  submitForm() {
    this.submitted = true;
    if (this.ProdutoForm.invalid) {
      alert('ERR0!! :-)\n\n' + JSON.stringify(this.ProdutoForm.value))
        return;
    }else{
      this.produtoService.createCliente(this.ProdutoForm.value).subscribe(res => {
        this.modalService.dismissAll();     
          this.ngZone.run(() => this.router.navigateByUrl('/ListProduto'))
      });
    }
  }
  BindTamanho(Tamanhocodigo : number){  
    console.log('teste tamanho'+Tamanhocodigo)
 this.tamanhoService.getTamanhosInt(Tamanhocodigo);  
 }  

 gettamanho(){
   
  this.tamanhos$= this.tamanhoService.getTamanhoList();
  console.log('teste') 
  console.log( this.tamanhos$)
 }
 getColecao(){
   
  this.colecao$= this.colecaoService.getColecaoList();
  console.log('teste colecao') 
  console.log( this.colecao$)
 
 }
 voltar(): void {
  this.modalService.dismissAll();
  localStorage.removeItem("editcodigoProduto"); 
 
  this.router.navigate(['ListProduto']);
  //this.router.navigate(['ListColecao']);  
};

}
