import { Component, OnInit, NgZone  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProdutoService } from '../../services/produto.service';
import { TamanhoService } from '../../services/tamanho.service';
import { ColecaoService } from '../../services/colecao.service';
import { Tamanho } from '../../tamanho/tamanho';
import { Observable } from 'rxjs';
import { Colecao } from 'src/app/colecao/colecao';
import { Produtos } from '../../produto/Produtos';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-edit-produto',
  templateUrl: './edit-produto.component.html',
  styleUrls: ['./edit-produto.component.scss']
})
export class EditProdutoComponent implements OnInit {
  produto: Produtos;
  ProdutoForm: FormGroup;
  IssueArr: any = [];
  submitted = false;
  ListTamanhos:Tamanho[];
  tamanhos$: Observable<Tamanho[]>;
  colecao$: Observable<Colecao[]>;
  errors: any[] = [];
  //date: {year: number, month: number};
  mudancasNaoSalvas: boolean;
  constructor(
      public fb: FormBuilder,
      private ngZone: NgZone,
      private router: Router,
      public produtoService: ProdutoService,
      public tamanhoService:TamanhoService,
      public colecaoService:ColecaoService,
      private modalService: NgbModal,
      private toastr: ToastrService,
      private spinner: NgxSpinnerService
      //public errorService: ErrorService
    ) {}

  ngOnInit() {
    this.gettamanho();
    this.getColecao();
    console.log('Issue editcodigoProduto!')
      this.addIssue()
      let empid = localStorage.getItem('editcodigoProduto');  
      if (+empid > 0) {  
        this.produtoService.getProdutosById(+empid).subscribe(data => {  
         // this.ProdutoForm.patchValue(data); 
          this.ProdutoForm.patchValue(
            {
              codigoProduto:data.codigoProduto,
              descricao:data.descricao,
              codigoTamanho:data.codigoTamanho,
              codigoColecao: data.codigoColecao,
              valor:data.valor,
           
           });  
         // this.ColecaoForm.patchValue(data)
        })  
        // this.btnvisibility = false;  
        // this.empformlabel = 'Edit Employee';  
        // this.empformbtn = 'Update';  
      } 
    
      //this.listtamanho()
}
/* Conver date object to string */

  addIssue() {
      this.ProdutoForm = this.fb.group({
        codigoProduto:[''],
        descricao: ['',Validators.required],
        codigoTamanho: ['',Validators.required],
        codigoColecao: ['',Validators.required],
        valor: ['',Validators.required],
      })
        
      
  }
  submitForm() {
    console.log('teste cadastrar')
    this.submitted = true;
    if (this.ProdutoForm.invalid) {
      alert('ERR0!! :-)\n\n' + JSON.stringify(this.ProdutoForm.value))
        return;
    }else{
      let produtoForm = Object.assign({}, this.produto, this.ProdutoForm.value);

      let id = localStorage.getItem('editcodigoProduto'); 
      var numberValue = Number(id); 
      this.spinner.show();
      this.produtoService.updateProduto(produtoForm).subscribe(
        sucesso => { this.processarSucesso(sucesso) },
        falha => { this.processarFalha(falha) }
        //this.modalService.dismissAll();
        //  this.ngZone.run(() => this.router.navigateByUrl('/ListProduto'))
      );
      //this.modalService.dismissAll();  
    }
  }
  BindTamanho(Tamanhocodigo : number){  
    console.log('teste tamanho'+Tamanhocodigo)
 this.tamanhoService.getTamanhosInt(Tamanhocodigo);  
 }  

 gettamanho(){
   
  this.tamanhos$= this.tamanhoService.getTamanhoList();
  console.log('teste') 
  console.log( this.tamanhos$)
 }
 getColecao(){
   
  this.colecao$= this.colecaoService.getColecaoList();
  console.log('teste colecao') 
  console.log( this.colecao$)
 
 }
 voltar(): void {
  this.modalService.dismissAll();
  localStorage.removeItem("editcodigoProduto"); 
 
  this.router.navigate(['ListProduto']);
  //this.router.navigate(['ListColecao']);  
};
processarFalha(fail: any) {
  this.errors = fail.error.errors;
  this.toastr.error('Ocorreu um erro!', 'Opa :(');
}
processarSucesso(response: any) {
  this.errors = [];    
  this.mudancasNaoSalvas = false;
 
  let toast = this.toastr.success('cadastrado com sucesso!', 'Sucesso!');
  if (toast) {
    toast.onHidden.subscribe(() => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
      this.spinner.hide();     
      this.ProdutoForm.reset();
      this.router.navigate(['ListProduto']);
      this.modalService.dismissAll();
      //this.router.navigate(['ListPedidoComponent']);
     // this.router.navigateByUrl('/ListPedidoComponent')
      }, 1000);
    });
  }
  
   
}
getFormattedPrice(price: number) {
  return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(price);
}
}
