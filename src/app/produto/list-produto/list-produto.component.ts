import { Component, OnInit,Inject,NgZone } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Produtos } from '../Produtos'
import { ProdutoService } from '../../services/produto.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { EditProdutoComponent } from '../edit-produto/edit-produto.component';
import { DetailProdutoComponent } from '../detail-produto/detail-produto.component';
import {debounceTime, map} from 'rxjs/operators';
@Component({
  selector: 'app-list-produto',
  templateUrl: './list-produto.component.html',
  styleUrls: ['./list-produto.component.scss']
})
export class ListProdutoComponent implements OnInit {
  ProdutoForm: FormGroup;
  public Produtos: Produtos[];
  Produtos$: Observable<Produtos[]>;
  public httpteste: HttpClient;
  public url: string;
  //public produtoService: ProdutoService;
  rows : any;
  config: any;
  collection = { count: 60, data: [] };
  public maxSize: number = 3;
  public directionLinks: boolean = true;
  public autoHide: boolean = false;
  public responsive: boolean = true;
  public labels: any = {
      previousLabel: '<--',
      nextLabel: '-->',
      screenReaderPaginationLabel: 'Pagination',
      screenReaderPageLabel: 'page',
      screenReaderCurrentLabel: `You're on page`
  };
  show: boolean = false;
  constructor(http: HttpClient,public produtoService: ProdutoService,  public fb: FormBuilder,
    private ngZone: NgZone,
    private router: Router,
    private modalService: NgbModal) {
      let quantidade =0 ;
      quantidade= this.collection.count;
      this.config = {
        itemsPerPage: 3,
        currentPage: 1,
        totalItems:this.collection.count
      };
   

}
pageChanged(event){
  console.log(event);
  this.config.currentPage = event;
}
  ngOnInit() {
    this.getdata();
  
    this.show = false;
  }
  getdata(){
    return( this.produtoService.getProdutosList().subscribe(result => {
     console.log(result)
      this.Produtos = result;
      this.rows = result;
    }, error => console.error(error)))
  }
  getdata2(){
    return( this.produtoService.getProdutosList().subscribe(result => {
     
      this.Produtos = result;
      
    }, error => console.error(error)))
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
   
   
    const temp = this.rows.filter(x => 
      
      x.descricao.toString().toLowerCase().includes(val.toString().toLowerCase())
      ||x.colecao.descricao.toString().toLowerCase().includes(val.toString().toLowerCase())
    
    )
    if(val && val.trim() != ''){
      this.rows = temp;
    }
    else{
      this.getdata()
      
    }
    
  
  
   //this.table.offset = 0;
  }
  delete(value){
    //s debugger
    //  this.selectedName = value;
    //  console.log("Add Delete Action code here" + value)
   }
   deleteUser(produtos: Produtos): void {
     localStorage.removeItem("editcodigoProduto");
     localStorage.setItem("editcodigoProduto", produtos.codigoProduto.toString());
    //  this.produtoService.deleteUser(produtos.codigoProduto)
    //    .subscribe( data => {
    //      this.router.navigate(['ListProduto']);  
    //     // this.Colecao = this.Colecao.filter(u => u !== colecao);
    //    })
   };
   onUpdate(produtos: Produtos) {  
     localStorage.removeItem("editcodigoProduto");
     localStorage.setItem("editcodigoProduto", produtos.codigoProduto.toString());
    //  this.produtoService.updateUser(this.ColecaoForm.value).subscribe(data => {  
    //    this.router.navigate(['ListColecao']);  
    //  },  
    //    error => {  
    //      alert(error);  
    //    });  
   }  
   edit(produtos: Produtos): void {
     console.log('edit'+produtos.codigoProduto)
     localStorage.removeItem("editcodigoProduto");
     localStorage.setItem("editcodigoProduto",produtos.codigoProduto.toString());
     const modalRef = this.modalService.open(EditProdutoComponent);
  
    modalRef.result.then((result) => {
      console.log(result);
      this.getdata();
    }).catch((error) => {
      console.log(error);
      this.getdata();
    });
     //this.router.navigate(['EditProduto']);
     //this.router.navigate(['ListColecao']);  
   };
   View(produtos: Produtos): void {
     console.log('views'+produtos.codigoProduto)
     localStorage.removeItem("editcodigoProduto");
     localStorage.setItem("editcodigoProduto", produtos.codigoProduto.toString());
     const modalRef = this.modalService.open(DetailProdutoComponent);
  
    modalRef.result.then((result) => {
      console.log(result);
     
    }).catch((error) => {
      console.log(error);
    
    });
     //this.router.navigate(['DetailProdutos']);
     //this.router.navigate(['ListColecao']);  
   };

   
  search = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    map(term => term === '' ? []
      : this.rows.filter(v => v.descricao.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
      
  )

formatter = (x: {descricao: string}) => x.descricao;
}
