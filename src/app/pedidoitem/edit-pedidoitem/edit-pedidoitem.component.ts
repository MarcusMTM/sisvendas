import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
//import {PedidoitemGestaoComponent} from '../../pedido/pedidoitem-gestao/pedidoitem-gestao.component'
@Component({
  selector: 'app-edit-pedidoitem',
  templateUrl: './edit-pedidoitem.component.html',
  styleUrls: ['./edit-pedidoitem.component.scss']
})
export class EditPedidoitemComponent implements OnInit {

  constructor( private modalService: NgbModal,
    public fb: FormBuilder, ) { }

  ngOnInit() {
  }
  voltar(): void {

    //localStorage.removeItem("editcodigoPedido"); 
    this.modalService.dismissAll();
    // const modalRef = this.modalService.open(PedidoitemGestaoComponent, { size: 'lg', backdrop: 'static' });
  
    // modalRef.result.then((result) => {
    //   console.log(result);
    // }).catch((error) => {
    //   console.log(error);
    // });
  };
}
