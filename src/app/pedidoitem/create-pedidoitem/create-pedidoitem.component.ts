import { Component, OnInit, NgZone,Input  } from '@angular/core';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { PedidoService } from '../../services/pedido.service';
import { PedidoitemService } from '../../services/pedidoitem.service';
import { Cliente } from '../../cliente/cliente';
import { Produtos } from '../../produto/Produtos';
import { Observable } from 'rxjs';
import { ClienteService } from '../../services/cliente.service';
import { ProdutoService } from '../../services/produto.service';
import { Pipe, PipeTransform } from '@angular/core';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { pedidoitem } from '../../pedidoitem/pedidoItem';
@Component({
  selector: 'app-create-pedidoitem',
  templateUrl: './create-pedidoitem.component.html',
  styleUrls: ['./create-pedidoitem.component.scss']
})
export class CreatePedidoitemComponent implements OnInit {
  PedidoItemForm: FormGroup;
  classgeneric: any = [];
  cliente$: Observable<Cliente[]>;
  produto$: Observable<Produtos[]>;
  ClientesSubscription: Subscription;
  ProdutosSubscription: Subscription;
  filteredOptions: Observable<Cliente[]>;
  filteredOptionsProdutos: Observable<Produtos[]>;
  options: Cliente[];
  rows = [];
  columns = []
  selected = [];
  constructor(
    public fb: FormBuilder,
    private ngZone: NgZone,
    private router: Router,
    public pedidoService: PedidoService,
    public clienteService: ClienteService,
    public produtoService:ProdutoService,
    public PedidoitemService:PedidoitemService
  ) { }

  ngOnInit() {
    console.log('TESTE');
    this.addIssue()
    this.getcliente()
    this.getProdutos()
  }
  addIssue() {


    this.PedidoItemForm = this.fb.group({
    
      dataPedido: [''],
      Codigoproduto: [''],
      codigoPedido: [''],
      quantidadeItem: [''],
      valoritem:[''],
      status:true
    })

    
}
submitForm() {
 

  console.log(this.PedidoItemForm.value);
  this.PedidoitemService.createpedidoitem(this.PedidoItemForm.value).subscribe(response => 
   {
  
    });
   
    
    
}
getcliente(){
   
  this.cliente$= this.clienteService.getClienteList();

 }

 getProdutos(){
   
  this.ProdutosSubscription= this.produtoService.getProdutosList().subscribe((produtos: Produtos[]) => {

    this.classgeneric = produtos;
 
});    

 }
 
 _filter(value: string): Cliente[] {
  const filterValue = value.toLowerCase();
  return this.options.filter(x =>
    x.nome.toUpperCase().indexOf(filterValue.toUpperCase()) !== -1);
  
}
}
