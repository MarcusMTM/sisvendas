import { Component, OnInit,Inject } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { pedidoitem } from '../pedidoItem';
import { PedidoitemService } from '../../services/pedidoitem.service';
import { Observable } from 'rxjs';
import {debounceTime, map,distinctUntilChanged} from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { EditPedidoitemComponent } from '../edit-pedidoitem/edit-pedidoitem.component';
import { DetailsPedidoitemComponent } from '../details-pedidoitem/details-pedidoitem.component';
@Component({
  selector: 'app-list-pedidoitem',
  templateUrl: './list-pedidoitem.component.html',
  styleUrls: ['./list-pedidoitem.component.scss']
})
export class ListPedidoitemComponent implements OnInit {
  public pedidoitem: pedidoitem[];
  public httpteste: HttpClient;
  public url: string;
  
  rows : any;
  Pedidoitem$: Observable<pedidoitem[]>;
  show: boolean = false;
  columnsWithSearch : string[] = [];
  constructor(http: HttpClient, public PedidoitemService: PedidoitemService,  private modalService: NgbModal,) {
    
    this.Pedidoitem$= this.PedidoitemService.getPedidosList();
    this.PedidoitemService.getPedidosList();
    this.getdata()

}

  ngOnInit() {
    console.log('getdata'+this.getdata())
    
  }
  getdata(){
    return( this.PedidoitemService.getPedidosList().subscribe(result => {
     
        this.pedidoitem = result;
        this.rows = result;
      }, error => console.error(error)))
  }
  public items: pedidoitem[];
  public searchTerm: string = "";
  temp = [];
 


updateFilter(event) {
  const val = event.target.value.toLowerCase();
 
 
  const temp = this.rows.filter(x => 
    
    x.codigoPedidoitem.toString().toLowerCase().includes(val.toString().toLowerCase())
    ||x.codigoPedido.toString().toLowerCase().includes(val.toString().toLowerCase())
    ||x.produto.descricao.toString().toLowerCase().includes(val.toString().toLowerCase())
  )
  if(val && val.trim() != ''){
    this.rows = temp;
  }
  else{
    this.getdata()
    
  }
  


 //this.table.offset = 0;
}
search = (text$: Observable<string>) =>
text$.pipe(
  debounceTime(200),
  distinctUntilChanged(),
  map(term => term === '' ? []
    : this.rows.filter(v => v.codigoPedidoitem.toString().indexOf(term.toLowerCase()) > -1 ||
    v.codigoPedido.toString().indexOf(term.toLowerCase()) > -1||
    v.produto.descricao.toString().indexOf(term.toLowerCase()) > -1
    ).slice(0, 10)
    
    )
    
)

formatter = (x: {pedidoitem: pedidoitem}) => x.pedidoitem;

edit(pedidoitem: pedidoitem): void {
  console.log('edit'+pedidoitem.codigoPedidoitem)
  localStorage.removeItem("editcodigoPedidoitem");
  localStorage.setItem("editcodigoPedidoitem", pedidoitem.codigoPedidoitem.toString());
  const modalRef = this.modalService.open(EditPedidoitemComponent,  { size: 'lg', backdrop: 'static' });

  modalRef.result.then((result) => {
    console.log(result);
  }).catch((error) => {
    console.log(error);
  });
 // this.router.navigate(['EditPedido']);
  //this.router.navigate(['ListColecao']);  
};
View(pedidoitem: pedidoitem): void {
  console.log('views'+pedidoitem.codigoPedidoitem)
  localStorage.removeItem("editcodigoPedidoitem");
  localStorage.setItem("editcodigoPedidoitem", pedidoitem.codigoPedidoitem.toString());
  const modalRef = this.modalService.open(DetailsPedidoitemComponent);

  modalRef.result.then((result) => {
    console.log(result);
  }).catch((error) => {
    console.log(error);
  });
 // this.router.navigate(['DetalhePedido']);
  //this.router.navigate(['ListColecao']);  
};
}
