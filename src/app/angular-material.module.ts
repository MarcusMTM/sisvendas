import { NgModule } from '@angular/core';
import { MatTabsModule,MatSliderModule,MatSidenavModule,MatIconModule, MatButtonModule, MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule   } from '@angular/material'


@NgModule({
    imports: [MatTabsModule,MatSliderModule,MatSidenavModule,MatIconModule, MatButtonModule, MatDatepickerModule,
        MatNativeDateModule,
        MatInputModule  ],
    exports: [MatTabsModule,MatSliderModule,MatSidenavModule,MatIconModule, MatButtonModule, MatDatepickerModule,
        MatNativeDateModule,
        MatInputModule  ]
})

export class AngularMaterialModule { }