import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  navbarOpen = false;
  show: boolean = false;
  //toggleMenu = true;
toggleNavbar() {
  this.navbarOpen = !this.navbarOpen;
  this.show=false;
}
  constructor() { }

  ngOnInit(): void {
  }

}
