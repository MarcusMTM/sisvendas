import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailTamanhoComponent } from './detail-tamanho.component';

describe('DetailTamanhoComponent', () => {
  let component: DetailTamanhoComponent;
  let fixture: ComponentFixture<DetailTamanhoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailTamanhoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailTamanhoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
