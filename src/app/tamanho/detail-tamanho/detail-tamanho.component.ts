import { Component, OnInit , NgZone  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TamanhoService } from '../../services/tamanho.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-detail-tamanho',
  templateUrl: './detail-tamanho.component.html',
  styleUrls: ['./detail-tamanho.component.scss']
})
export class DetailTamanhoComponent implements OnInit {

  submitted = false;
  TamanhoForm: FormGroup;
 
  constructor(
      public fb: FormBuilder,
      private ngZone: NgZone,
      private router: Router,
      public tamanhoservices: TamanhoService,
      private modalService: NgbModal
    ) {}

  ngOnInit() {
    console.log('teste')
      this.addIssue()
      let empid = localStorage.getItem('editcodigoTamanho');  
      if (+empid > 0) {  
        this.tamanhoservices.getTamanhoById(+empid).subscribe(data => {  
       
          this.TamanhoForm.patchValue(data);  
        })  
        // this.btnvisibility = false;  
        // this.empformlabel = 'Edit Employee';  
        // this.empformbtn = 'Update';  
      } 
}
/* Conver date object to string */

  addIssue() {
      this.TamanhoForm = this.fb.group({
        descricao: [{value: '', disabled: true},Validators.required]
        
      });
  }
  submitForm() {
    this.submitted = true;
    if (this.TamanhoForm.invalid) {
      alert('ERR0!! :-)\n\n' + JSON.stringify(this.TamanhoForm.value))
        return;
    }else{
      
      this.tamanhoservices.createTamanhos(this.TamanhoForm.value).subscribe(res => {
        this.modalService.dismissAll();  
          this.ngZone.run(() => this.router.navigateByUrl('/ListTamanho'))
      });
  }
}
voltar(): void {
  localStorage.removeItem("editcodigoTamanho");
  this.modalService.dismissAll();

  this.router.navigate(['ListTamanho']);  
};

}
