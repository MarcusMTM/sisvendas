import { Component, OnInit,Inject,NgZone } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Tamanho } from '../tamanho';
import { TamanhoService } from '../../services/tamanho.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {DetailTamanhoComponent} from '../detail-tamanho/detail-tamanho.component'
import {EditTamanhoComponent} from '../edit-tamanho/edit-tamanho.component'
import {debounceTime, map} from 'rxjs/operators';
@Component({
  selector: 'app-list-tamanho',
  templateUrl: './list-tamanho.component.html',
  styleUrls: ['./list-tamanho.component.scss']
})
export class ListTamanhoComponent implements OnInit {
  tamanhos$: Observable<Tamanho[]>;
  public tamanhos: Tamanho[];
  public httpteste: HttpClient;
  public url: string;
  rows : any;

  show: boolean = false;
  constructor(http: HttpClient,public TamanhoService: TamanhoService,public fb: FormBuilder,
    private ngZone: NgZone,
    private router: Router,
    private modalService: NgbModal) {
 
  // this.gettamanho();

}

  ngOnInit() {
    this.show = false;
    this.gettamanho();
  }
  gettamanho(){
   
    return(   this.TamanhoService.getTamanhoList().subscribe(result => {
    this.tamanhos = result;
    this.rows = result;
  }, error => console.error(error)))


  }
  getdata(){
    return( this.TamanhoService.getTamanhoList().subscribe(result => {
     
      this.tamanhos = result;
      this.rows = result;
    }, error => console.error(error)))
  }
  delete(value){
    //s debugger
    //  this.selectedName = value;
    //  console.log("Add Delete Action code here" + value)
   }
   deleteUser(tamanho: Tamanho): void {
     localStorage.removeItem("editcodigoTamanho");
     localStorage.setItem("editcodigoTamanho", tamanho.codigotamanho.toString());
    //  this.produtoService.deleteUser(produtos.codigoProduto)
    //    .subscribe( data => {
    //      this.router.navigate(['ListProduto']);  
    //     // this.Colecao = this.Colecao.filter(u => u !== colecao);
    //    })
   };
   onUpdate(tamanho: Tamanho) {  
     localStorage.removeItem("editcodigoTamanho");
     localStorage.setItem("editcodigoTamanho", tamanho.codigotamanho.toString());
    //  this.produtoService.updateUser(this.ColecaoForm.value).subscribe(data => {  
    //    this.router.navigate(['ListColecao']);  
    //  },  
    //    error => {  
    //      alert(error);  
    //    });  
   }  
   edit(tamanho: Tamanho): void {
     console.log('edit'+tamanho.codigotamanho)
     localStorage.removeItem("editcodigoTamanho");
     localStorage.setItem("editcodigoTamanho",tamanho.codigotamanho.toString());
     const modalRef = this.modalService.open(EditTamanhoComponent);
  
     modalRef.result.then((result) => {
       console.log(result);
       this.getdata()
     }).catch((error) => {
       console.log(error);
       this.getdata()
     });
    // this.router.navigate(['EditTamanho']);
    
   };
   View(tamanho: Tamanho): void {
    console.log('edit'+tamanho.codigotamanho)
     localStorage.removeItem("editcodigoTamanho");
     localStorage.setItem("editcodigoTamanho",tamanho.codigotamanho.toString());
     const modalRef = this.modalService.open(DetailTamanhoComponent);
  
     modalRef.result.then((result) => {
       console.log(result);
     }).catch((error) => {
       console.log(error);
     });
    // this.router.navigate(['DetailTamanho']);
    
   };

   updateFilter(event) {
    const val = event.target.value.toLowerCase();
   
   
    const temp = this.rows.filter(x => 
      
      x.descricao.toString().toLowerCase().includes(val.toString().toLowerCase())
    
   
    )
    if(val && val.trim() != ''){
      this.rows = temp;
    }
    else{
      this.getdata()
      
    }
  }
   search = (text$: Observable<string>) =>
   text$.pipe(
     debounceTime(200),
     map(term => term === '' ? []
       : this.rows.filter(v => v.descricao.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
       
   )
 
 formatter = (x: {descricao: string}) => x.descricao;
}
