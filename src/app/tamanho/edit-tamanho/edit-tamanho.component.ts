import { Component, OnInit,NgZone  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TamanhoService } from '../../services/tamanho.service';
import { Tamanho } from '../../tamanho/tamanho';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-edit-tamanho',
  templateUrl: './edit-tamanho.component.html',
  styleUrls: ['./edit-tamanho.component.scss']
})
export class EditTamanhoComponent implements OnInit {
  reactiveForm: FormGroup;
  submitted = false;
  TamanhoForm: FormGroup;
  IssueArr: any = [];
  tamanho: Tamanho;
  //date: {year: number, month: number};
  errors: any[] = [];
  //date: {year: number, month: number};
  mudancasNaoSalvas: boolean;
  constructor(
      public fb: FormBuilder,
      private ngZone: NgZone,
      private router: Router,
      public tamanhoservices: TamanhoService,
      private modalService: NgbModal,
      private toastr: ToastrService,
      private spinner: NgxSpinnerService
    ) {}

  ngOnInit() {
  
      this.addIssue()
      let empid = localStorage.getItem('editcodigoTamanho');  
      if (+empid > 0) {  
        this.tamanhoservices.getTamanhoById(+empid).subscribe(data => {  
          
          this.TamanhoForm.patchValue(data);  
        })  
     
      } 
}
/* Conver date object to string */

  addIssue() {
      this.TamanhoForm = this.fb.group({
        codigotamanho:[''],
        descricao: ['',Validators.required]
        
      });
  }
  submitForm() {
    this.submitted = true;
    if (this.TamanhoForm.invalid) {
      alert('ERR0!! :-)\n\n' + JSON.stringify(this.TamanhoForm.value))
        return;
    }else{
      let tamanhoForm = Object.assign({}, this.tamanho, this.TamanhoForm.value);
      this.spinner.show();
      this.tamanhoservices.updateTamanho(tamanhoForm).subscribe(//res => {
       // this.modalService.dismissAll();
        sucesso => { this.processarSucesso(sucesso) },
        falha => { this.processarFalha(falha) }
         // this.ngZone.run(() => this.router.navigateByUrl('/ListTamanho'))
      //}
      );
  }
}
voltar(): void {
  localStorage.removeItem("editcodigoTamanho");
  this.modalService.dismissAll();

  this.router.navigate(['ListTamanho']);  
};
processarFalha(fail: any) {
  this.errors = fail.error.errors;
  this.toastr.error('Ocorreu um erro!', 'Opa :(');
}
processarSucesso(response: any) {
  
  this.errors = [];

  this.mudancasNaoSalvas = false;

  let toast = this.toastr.success('cadastrado com sucesso!', 'Sucesso!');
  if (toast) {
    toast.onHidden.subscribe(() => {  
      setTimeout(() => {       
      this.spinner.hide(); 
      this.router.navigate(['ListTamanho']);
      this.modalService.dismissAll();
      }, 1000);
    });
    
  }
}
}
