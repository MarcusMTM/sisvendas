import { Component, OnInit, NgZone  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TamanhoService } from '../../services/tamanho.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-create-tamanho',
  templateUrl: './create-tamanho.component.html',
  styleUrls: ['./create-tamanho.component.scss']
})
export class CreateTamanhoComponent implements OnInit {

  reactiveForm: FormGroup;
  submitted = false;
  TamanhoForm: FormGroup;
  IssueArr: any = [];
  errors: any[] = [];
  //date: {year: number, month: number};
  mudancasNaoSalvas: boolean;
  constructor(
      public fb: FormBuilder,
      private ngZone: NgZone,
      private router: Router,
      public tamanhoservices: TamanhoService,
      private toastr: ToastrService,
      private spinner: NgxSpinnerService
      //public errorService: ErrorService
    ) {}

  ngOnInit() {
  
      this.addIssue()
}
/* Conver date object to string */

  addIssue() {
      this.TamanhoForm = this.fb.group({
        descricao: ['',Validators.required]
        
      });
  }
  submitForm() {
    this.submitted = true;
    if (this.TamanhoForm.invalid) {
      this.errors =this.TamanhoForm.value;
      this.toastr.error('Ocorreu no cadastro!', JSON.stringify(this.TamanhoForm.value))
     // alert('ERR0!! :-)\n\n' + JSON.stringify(this.TamanhoForm.value))
        return;
    }else{
      this.spinner.show();
      this.tamanhoservices.createTamanhos(this.TamanhoForm.value).subscribe( 
        sucesso => { this.processarSucesso(sucesso) },
      falha => { this.processarFalha(falha) }
      );
  }
}
voltar(): void {
  localStorage.removeItem("editcodigoTamanho");
 

  this.router.navigate(['ListTamanho']);  
};
processarFalha(fail: any) {
  this.errors = fail.error.errors;
  this.toastr.error('Ocorreu um erro!', 'Opa :(');
}
processarSucesso(response: any) {
  this.TamanhoForm.reset();
  this.errors = [];

  this.mudancasNaoSalvas = false;

  let toast = this.toastr.success('cadastrado com sucesso!', 'Sucesso!');
  if (toast) {
    toast.onHidden.subscribe(() => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
     
      this.TamanhoForm.reset();
      this.router.navigate(['ListTamanho']);
      }, 1000);
     
    });
  }
}
}
