import { Component, OnInit,Inject,NgZone } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Cliente } from '../cliente';
import { ClienteService } from '../../services/cliente.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {DetailClienteComponent} from '../detail-cliente/detail-cliente.component'
import {EditClienteComponent} from '../edit-cliente/edit-cliente.component'
import {debounceTime, map} from 'rxjs/operators';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-list-cliente',
  templateUrl: './list-cliente.component.html',
  styleUrls: ['./list-cliente.component.scss']
})
export class ListClienteComponent implements OnInit {
  public Clientes: Cliente[];
  public httpteste: HttpClient;
  public url: string;
  
  rows : any;
  show: boolean = false;
  constructor(http: HttpClient,public ClienteServices: ClienteService,   private ngZone: NgZone,
    private router: Router,  public fb: FormBuilder,
    private modalService: NgbModal) {
   // var teste= this.UsuarioservicesService.getUsuariologin();
   //  console.log(teste); this.getdata()
   this.getdata()

}

  ngOnInit() {
    this.show = false;
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
   
   
    const temp = this.rows.filter(x => 
      
      x.nome.toString().toLowerCase().includes(val.toString().toLowerCase())
   
    )
    if(val && val.trim() != ''){
      this.rows = temp;
    }
    else{
      this.getdata()
      
    }
  }
   
 
  
  deleteUser(clientes: Cliente): void {
    localStorage.removeItem("editcodigoCliente");
    localStorage.setItem("editcodigoCliente", clientes.codigoCliente.toString());
    // this.ClienteServices.deleteUser(clientes.codigoCliente)
    //   .subscribe( data => {
    //     this.router.navigate(['ListColecao']);  
    //    // this.Colecao = this.Colecao.filter(u => u !== colecao);
    //   })
  };
  onUpdate(clientes: Cliente) {  
    localStorage.removeItem("editcodigoCliente");
    localStorage.setItem("editcodigoCliente", clientes.codigoCliente.toString());
    // this.ClienteServices.updateUser(this.ColecaoForm.value).subscribe(data => {  
    //   this.router.navigate(['ListColecao']);  
    // },  
    //   error => {  
    //     alert(error);  
    //   });  
  }  
  edit(clientes: Cliente): void {
    console.log('edit'+clientes.codigoCliente)
    localStorage.removeItem("editcodigoCliente");
    localStorage.setItem("editcodigoCliente", clientes.codigoCliente.toString());
    const modalRef = this.modalService.open(EditClienteComponent);
  
    modalRef.result.then((result) => {
      console.log(result);
      this.getdata()
    }).catch((error) => {
      console.log(error);
      this.getdata()
    });
    //this.router.navigate(['EditCliente']);
    //this.router.navigate(['ListColecao']);  
  };
  View(clientes: Cliente): void {
    console.log('views'+clientes.codigoCliente)
    localStorage.removeItem("editcodigoCliente");
    localStorage.setItem("editcodigoCliente", clientes.codigoCliente.toString());
    const modalRef = this.modalService.open(DetailClienteComponent);
  
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
    //this.router.navigate(['DetailCliente']);
    //this.router.navigate(['ListColecao']);  
  };
  getdata(){
    return( this.ClienteServices.getClienteList().subscribe(result => {
     
      this.Clientes = result;
      this.rows = result;
    }, error => console.error(error)))
  }

  
  search = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    map(term => term === '' ? []
      : this.rows.filter(v => v.nome.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
      
  )

formatter = (x: {nome: string}) => x.nome;
}
