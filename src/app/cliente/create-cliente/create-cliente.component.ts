import { Component, OnInit, NgZone  } from '@angular/core';
import { FormBuilder, FormGroup,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { ClienteService } from '../../services/cliente.service';
import { MASKS, NgBrazilValidators } from 'ng-brazil';
import { ToastrService } from 'ngx-toastr';

import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-create-cliente',
  templateUrl: './create-cliente.component.html',
  styleUrls: ['./create-cliente.component.scss']
})
export class CreateClienteComponent implements OnInit {
  reactiveForm: FormGroup;
  submitted = false;
    ClientesForm: FormGroup;
    IssueArr: any = [];
    errors: any[] = [];
    //date: {year: number, month: number};
    mudancasNaoSalvas: boolean;
    //date: {year: number, month: number};
    public MASKS = MASKS;
    constructor(
        public fb: FormBuilder,
        private ngZone: NgZone,
        private router: Router,
        public clienteService: ClienteService,
        private toastr: ToastrService,
        private spinner: NgxSpinnerService
      ) {}

    ngOnInit() {
      console.log('Issue added!')
        this.addIssue()
  }
/* Conver date object to string */

    addIssue() {
        this.ClientesForm = this.fb.group({
            nome: ['',Validators.required],
            dataNascimento: ['',Validators.required],
            instagram: ['',Validators.required],
            whatsapp: ['', [<any>Validators.required, <any>NgBrazilValidators.telefone]],
            email: ['',Validators.required],
        });
    }
    submitForm() {
      this.submitted = true;
      if (this.ClientesForm.valid) {
        this.spinner.show();
        this.clienteService.createCliente(this.ClientesForm.value).subscribe(
          sucesso => { this.processarSucesso(sucesso) },
          falha => { this.processarFalha(falha) }
            //this.ngZone.run(() => this.router.navigateByUrl('/ListCliente'))
        );
      }
        else{
          return;
        }
      
    }
    voltar(): void {

      localStorage.removeItem("editcodigoCliente"); 
     
      this.router.navigate(['ListCliente']);
      //this.router.navigate(['ListColecao']);  
    };
    processarFalha(fail: any) {
      this.errors = fail.error.errors;
      this.toastr.error('Ocorreu um erro!', 'Opa :(');
    }
    processarSucesso(response: any) {
      this.errors = [];    
      this.mudancasNaoSalvas = false;
    
      let toast = this.toastr.success('cadastrado com sucesso!', 'Sucesso!');
      if (toast) {
        toast.onHidden.subscribe(() => {
          setTimeout(() => {
            /** spinner ends after 5 seconds */
            this.spinner.hide();      
        
          this.router.navigate(['ListCliente']);
          }, 1000);
        
        });
      }
      this.ClientesForm.reset();
      
    }
}
