export interface Cliente {
    codigoCliente: number;
	nome: string;
	dataNascimento: Date ;
	instagram :string;
	whatsapp :string;
	email :string;
}
