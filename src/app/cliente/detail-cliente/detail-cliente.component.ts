import { Component, OnInit,NgZone  } from '@angular/core';
import { FormBuilder, FormGroup,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { ClienteService } from '../../services/cliente.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-detail-cliente',
  templateUrl: './detail-cliente.component.html',
  styleUrls: ['./detail-cliente.component.scss']
})
export class DetailClienteComponent implements OnInit {

  submitted = false;
    ClientesForm: FormGroup;
    IssueArr: any = [];
    
    //date: {year: number, month: number};
   
    constructor(
        public fb: FormBuilder,
        private ngZone: NgZone,
        private router: Router,
        public clienteService: ClienteService,
        private modalService: NgbModal
    
        //public errorService: ErrorService
      ) {}

    ngOnInit() {
  
        this.addIssue()
        let id = localStorage.getItem('editcodigoCliente');  
        if (+id > 0) {  
          this.clienteService.getClienteById(+id).subscribe(data => {  
         
            this.ClientesForm.patchValue(data);  
          })  
          // this.btnvisibility = false;  
          // this.empformlabel = 'Edit Employee';  
          // this.empformbtn = 'Update';  
        } 
  }
/* Conver date object to string */

    addIssue() {
        this.ClientesForm = this.fb.group({
            nome: [{value: '', disabled: true},Validators.required],
            dataNascimento: [{value: '', disabled: true},Validators.required],
            instagram: [{value: '', disabled: true},Validators.required],
            whatsapp: [{value: '', disabled: true},Validators.required],
            email: [{value: '', disabled: true},Validators.required],
        });
    }
    submitForm() {
      this.submitted = true;
      if (this.ClientesForm.valid) {
        this.clienteService.createCliente(this.ClientesForm.value).subscribe(res => {
           console.log('Issue added!')
           
            this.ngZone.run(() => this.router.navigateByUrl('/ListCliente'))
        });
      }
        else{
          return;
        }
      
    }
    voltar(): void {

      localStorage.removeItem("editcodigoCliente"); 
      this.modalService.dismissAll();  
      this.router.navigate(['ListCliente']);
      //this.router.navigate(['ListColecao']);  
    };

}
