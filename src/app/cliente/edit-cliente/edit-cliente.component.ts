import { Component, OnInit,NgZone  } from '@angular/core';
import { FormBuilder, FormGroup,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { ClienteService } from '../../services/cliente.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { DatePipe,CurrencyPipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-edit-cliente',
  templateUrl: './edit-cliente.component.html',
  styleUrls: ['./edit-cliente.component.scss']
})
export class EditClienteComponent implements OnInit {
  pipe = new DatePipe('en-US');
  currentDate = new Date().toISOString().substring(0, 10);
  submitted = false;
    ClientesForm: FormGroup;
    IssueArr: any = [];
    errors: any[] = [];
    //date: {year: number, month: number};
    mudancasNaoSalvas: boolean;
    constructor(
        public fb: FormBuilder,
        private ngZone: NgZone,
        private router: Router,
        public clienteService: ClienteService,
        private modalService: NgbModal,
        private toastr: ToastrService,
        private spinner: NgxSpinnerService
      ) {}

    ngOnInit() {
     
      
        this.addIssue()
        let id = localStorage.getItem('editcodigoCliente');  
        if (+id > 0) {  
          this.clienteService.getClienteById(+id).subscribe(data => {  
            console.log(this.pipe.transform(data.dataNascimento));
            this.ClientesForm.patchValue({
              codigoCliente:data.codigoCliente,
              nome:data.nome,
              dataNascimento:this.pipe.transform(data.dataNascimento,'yyyy-MM-dd'),//this.pipe.transform(data.dataNascimento, 'dd/MM/yyyy'),//'dd/MM/yyyy') //new Date().toISOString().substring(0, 10),//,
              instagram:data.instagram,
              whatsapp:data.whatsapp,
              email:data.email
              });  
          })  
          // this.btnvisibility = false;  
          // this.empformlabel = 'Edit Employee';  
          // this.empformbtn = 'Update';  
        } 
  }
/* Conver date object to string */

    addIssue() {
        this.ClientesForm = this.fb.group({
          codigoCliente: [''],
            nome: ['',Validators.required],
            dataNascimento: ['',Validators.required],
            instagram: ['',Validators.required],
            whatsapp: ['',Validators.required],
            email: ['',Validators.required],
        });
    }
    submitForm() {
      this.submitted = true;
     
      if (this.ClientesForm.valid) {
        //this.ClientesForm.patchValue({dataNascimento:this.pipe.transform(this.dataNascimento, 'dd/MM/yyyy'), });
        this.clienteService.updateCliente(this.ClientesForm.value).subscribe(
          sucesso => { this.processarSucesso(sucesso) },
          falha => { this.processarFalha(falha) }
         
           
            //this.ngZone.run(() => this.router.navigateByUrl('/ListCliente'))
        );
       // this.modalService.dismissAll();  
      }
        else{
          return;
        }
      
    }
    voltar(): void {

      localStorage.removeItem("editcodigoCliente"); 
      this.modalService.dismissAll();  
      this.router.navigate(['ListCliente']);
      //this.router.navigate(['ListColecao']);  
    };
    processarFalha(fail: any) {
      this.errors = fail.error.errors;
      this.toastr.error('Ocorreu um erro!', 'Opa :(');
    }
    processarSucesso(response: any) {
      this.spinner.show();
      this.errors = [];    
      this.mudancasNaoSalvas = false;
    
      let toast = this.toastr.success('cadastrado com sucesso!', 'Sucesso!');
      if (toast) {
        toast.onHidden.subscribe(() => {
         // this.router.navigate(['ListCliente']);
          setTimeout(() => {
            /** spinner ends after 5 seconds */
          this.spinner.hide();     
          this.ClientesForm.reset();
          this.router.navigate(['ListCliente']);
          this.modalService.dismissAll();
          //this.router.navigate(['ListPedidoComponent']);
         // this.router.navigateByUrl('/ListPedidoComponent')
          }, 1000);
        });
      }
     // this.ClientesForm.reset();
      
    }
}
