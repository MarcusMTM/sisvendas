export class LocalStorageUtils {
    
    public obterUsuario() {
        return JSON.parse(localStorage.getItem('userToken.nomeusuario'));
    }

    public salvarDadosLocaisUsuario(response: any) {
        this.salvarTokenUsuario(response.accessToken);
        this.salvarUsuario(response.userToken);
    }

    public limparDadosLocaisUsuario() {
        localStorage.removeItem('accessToken');
        localStorage.removeItem('userToken.nomeusuario');
    }

    public obterTokenUsuario(): string {
        return localStorage.getItem('accessToken');
    }

    public salvarTokenUsuario(token: string) {
        localStorage.setItem('accessToken', token);
    }

    public salvarUsuario(user: string) {
        localStorage.setItem('userToken.nomeusuario', JSON.stringify(user));
    }

}