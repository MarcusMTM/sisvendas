export interface Usuario {
    codigoUsuario: number;
    nomeusuario: string;
    cpf: string;
    login: string;
    senha: string;
    Boolean: boolean;
    email: string;
  }