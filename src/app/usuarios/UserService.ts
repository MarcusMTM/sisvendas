import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { Usuario } from '../usuarios/usuario';

import { BaseService } from '../services/baseService';


@Injectable()
export class UserService extends BaseService {

    constructor(private http: HttpClient) { super() }

    login(user: Usuario): Observable<Usuario> {

        return this.http
            .post(`${this.UrlServiceV1}login`, user, super.ObterHeaderJson())
            .pipe(
                map(super.extractData),
                catchError(super.serviceError)
            );
    }

    persistirUserApp(response: any){
        console.log('teste acess'+JSON.stringify(response));
        localStorage.setItem('accessToken', response.accessToken);
        localStorage.setItem('userToken.nomeusuario', JSON.stringify(response.userToken.nomeusuario));
    }
}