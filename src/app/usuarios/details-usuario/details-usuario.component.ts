import { Component, OnInit,NgZone  } from '@angular/core';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioservicesService } from '../../services/usuarioservices.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-details-usuario',
  templateUrl: './details-usuario.component.html',
  styleUrls: ['./details-usuario.component.scss']
})
export class DetailsUsuarioComponent implements OnInit {

  UsuarioForm: FormGroup;
    IssueArr: any = [];
    submitted = false;
    constructor(
        public fb: FormBuilder,
        private ngZone: NgZone,
        private router: Router,
        public usuarioService: UsuarioservicesService,
        private modalService: NgbModal
      ) {}

  ngOnInit() {
 
       this.addIssue()
      let empid = localStorage.getItem('editcodigoUsuario');  
      if (+empid > 0) {  
        this.usuarioService.getUsuarioById(+empid).subscribe(data => {  
       
          this.UsuarioForm.patchValue(data);  
        })  
        // this.btnvisibility = false;  
        // this.empformlabel = 'Edit Employee';  
        // this.empformbtn = 'Update';  
      } 
}
/* Conver date object to string */

  addIssue() {
    this.UsuarioForm = this.fb.group({
      nomeusuario: [{value: '', disabled: true}, Validators.required],
      login: [{value: '', disabled: true}, Validators.required],
      senha: [{value: '', disabled: true}, Validators.required,Validators.minLength(3)],
       cpf: [{value: '', disabled: true},Validators.required]
  });
  }
  submitForm() {
    
    this.router.navigateByUrl('listUsuario');
    
  
}
  voltar() {
    this.modalService.dismissAll();
    this.router.navigateByUrl('listUsuario');
 
  
}

}
