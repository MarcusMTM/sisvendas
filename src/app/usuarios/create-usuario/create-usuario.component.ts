import { Component, OnInit, NgZone,ElementRef,ViewChild, ViewChildren, AfterViewInit  } from '@angular/core';
import { FormBuilder, FormGroup,Validator, Validators,FormControlName } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioservicesService } from '../../services/usuarioservices.service';
import { ToastrService } from 'ngx-toastr';
import { MASKS, NgBrazilValidators } from 'ng-brazil';
import { Observable, fromEvent, merge } from 'rxjs';
import { ValidationMessages, GenericValidator, DisplayMessage } from '../../utils/GenericValidator';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-create-usuario',
  templateUrl: './create-usuario.component.html',
  styleUrls: ['./create-usuario.component.scss']
})

export class CreateUsuarioComponent implements OnInit,AfterViewInit {
// @ViewChild('input') nameField:ElementRef;
// @ViewChildren('formRow') rows: ElementRef;
// // ngAfterViewInit() {
// //       setTimeout(() => this.inputEl.nativeElement.focus());
// // }
//   @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];
  UsuarioForm: FormGroup;
  IssueArr: any = [];
  errors: any[] = [];
  //date: {year: number, month: number};
  mudancasNaoSalvas: boolean;
  public MASKS = MASKS;
    submitted = false;
    validationMessages: ValidationMessages;
    genericValidator: GenericValidator;
    displayMessage: DisplayMessage = {};
    constructor(
        public fb: FormBuilder,
        private ngZone: NgZone,
        private router: Router,
        public usuarioService: UsuarioservicesService,
        private toastr: ToastrService,
        private el: ElementRef,
        private spinner: NgxSpinnerService
        
      ) {}

    ngOnInit() {
       //
        this.addIssue()
  
  }
  ngAfterViewInit(): void {
    

    //this.el.nativeElement.focus();
    
    // let controlBlurs: Observable<any>[] = this.formInputElements
    //   .map((formControl: ElementRef) => fromEvent(formControl.nativeElement, 'focus'));
    //   console.log('controlBlurs')
    //  merge(...controlBlurs).subscribe(() => {
      
    //    this.displayMessage = this.genericValidator.processarMensagens(this.UsuarioForm);
    //    this.mudancasNaoSalvas = true;
    // console.log('controlBlurs')
    // });
  }
    addIssue() {
        this.UsuarioForm = this.fb.group({
            nomeusuario: ['', Validators.required],
            login: ['', Validators.required],
            senha: ['', Validators.required,Validators.minLength(3)],
             cpf:  ['', Validators.required],
             status: ['', Validators.required]
        })
    }
    submitForm() {
      if(this.UsuarioForm.dirty ){
      this.submitted = true;

      this.spinner.show();
        this.usuarioService.createUsuario(this.UsuarioForm.value).subscribe( 
            sucesso => { this.processarSucesso(sucesso) },
             falha => { this.processarFalha(falha) }
            //this.ngZone.run(() => this.router.navigateByUrl('/listUsuario'))
        );
        }
  
    }
   
    voltar(): void {
      localStorage.removeItem("editcodigoProduto");
     
    
      this.router.navigate(['listUsuario']);  
    };

    processarFalha(fail: any) {
      this.errors = fail.error.errors;
      this.toastr.error('Ocorreu um erro!', 'Opa :(');
    }
    processarSucesso(response: any) {
     
      this.errors = [];
    
      this.mudancasNaoSalvas = false;
    
      let toast = this.toastr.success('cadastrado com sucesso!', 'Sucesso!');
      if (toast) {
        toast.onHidden.subscribe(() => {
        
         // this.router.navigate(['listUsuario']);
         
          setTimeout(() => {
            /** spinner ends after 5 seconds */
            this.spinner.hide();
         
          this.UsuarioForm.reset();
          this.router.navigate(['listUsuario']);
          }, 1000);
        });
        this.UsuarioForm.reset();
      }
    }
}
