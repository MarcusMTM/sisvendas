import { Component, OnInit } from '@angular/core';
import { Usuario } from '../usuario';
import { UsuarioservicesService } from '../../services/usuarioservices.service';
import { LocalStorageUtils } from '../../utils/LocalStorageUtils';
import { Router } from '@angular/router';
@Component({
  selector: 'app-menu-user',
  templateUrl: './menu-user.component.html',
  styleUrls: ['./menu-user.component.scss']
})
export class MenuUserComponent  {

  token: string = "";
  user: any;
  email: string = "";
 saudacao: string;
 localStorageUtils = new LocalStorageUtils();
  constructor(private userService: UsuarioservicesService,private router: Router) {  }


 
  userLogado(): boolean {
    this.token = this.localStorageUtils.obterTokenUsuario();
    this.user = this.localStorageUtils.obterUsuario();
  
    var user = this.userService.obterUsuario();
    
    if (user) {
     
      this.saudacao = "Olá " + user;
      return true;
    }

    return false;
  }
  logout() {
    this.localStorageUtils.limparDadosLocaisUsuario();
    this.router.navigate(['/HomeComponent']);
  }
 
}
