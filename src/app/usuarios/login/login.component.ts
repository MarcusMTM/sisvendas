import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { Usuario } from '../usuario';
import { UserService } from '../userService';
import { User } from '../user';
import { UsuarioservicesService } from '../../services/usuarioservices.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userForm: FormGroup;
  user: Usuario;
  users: User;
  errors: any[] = [];

  constructor(private fb: FormBuilder,
    private router: Router,
    private userService: UsuarioservicesService,
    private userServices:UserService ,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.addlogin()
    
  }
  addlogin() {
    this.userForm = this.fb.group({
      login: ['', Validators.required],
      senha: ['', Validators.required],
    })
} 
  login() {
    if (this.userForm.valid && this.userForm.dirty) {
      this.spinner.show();
      let _user = Object.assign({}, this.user, this.userForm.value);
      //this.toastr.success('Hello world!', 'Toastr fun!');
      this.userService.getlogin(_user)
        .subscribe(
          
          result => { this.onSaveComplete(result) }
          ,
          fail => { this.onError(fail) }
        );
    }
  }

  onSaveComplete(response: any) {
    
    this.userForm.reset();
    this.errors = [];

    this.userServices.persistirUserApp(response);

    let toast = this.toastr.success('Login realizado com Sucesso!', 'Bem vindo!!!');
    if(toast){
      toast.onHidden.subscribe(() => {
        setTimeout(() => {
          /** spinner ends after 5 seconds */
          this.spinner.hide();
       
          this.router.navigate(['/HomeComponent']);
        }, 1000);
      
      });
    }
   // this.router.navigateByUrl('/HomeComponent');
  }

  onError(fail: any) {
    this.spinner.hide();
    this.errors = fail.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Opa :(');
     this.router.navigate(['/HomeComponent']);
  }

  processarSucesso(response: any) {

   // this.contaService.LocalStorage.salvarDadosLocaisUsuario(response);

  
  }

  processarFalha(fail: any){
   
  }
}
