import { Component, OnInit,Inject,ViewChild,NgZone } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Usuario } from '../usuario';
import { UsuarioservicesService } from '../../services/usuarioservices.service';
import { Observable } from 'rxjs'; 
import { DataTableDirective } from 'angular-datatables';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {debounceTime, map} from 'rxjs/operators';
import { EditUsuarioComponent } from '../edit-usuario/edit-usuario.component';
import { DetailsUsuarioComponent } from '../details-usuario/details-usuario.component';
@Component({
  selector: 'app-list-usuario',
  templateUrl: './list-usuario.component.html',
  styleUrls: ['./list-usuario.component.scss']
})

export class ListUsuarioComponent implements OnInit {
  public usuarios: Usuario[];
  public httpteste: HttpClient;
  public url: string;
  
  public cacheUsuarios: Usuario[];
  public Nomeusuario: Usuario[];
  private _allUsuario: Observable<Usuario[]>; 
  show: boolean = false;
  rows : any;
  Selnomeusuario:string ="";
  page = 1;
  pageSize = 4;
  collectionSize = 0;
  @ViewChild(DataTableDirective, {static: false})
  private datatableElement: DataTableDirective;
  closeResult = '';
 transform(value: any, args?: any): any {
  // added code
  if(args == null){
    return value;
   }
 // added code

   return value.filter(
     item => item.nomeusuario.toLowerCase().indexOf(args.toLowerCase()) > -1
  );
 }
 dtOptions: DataTables.Settings = {};
  constructor(http: HttpClient,public UsuarioservicesService: UsuarioservicesService,
    public fb: FormBuilder,
    private ngZone: NgZone,
    private router: Router,
    private modalService: NgbModal) {
    this.getdata()


}

buscanome()  
  {  
    debugger;  
    this._allUsuario=this.UsuarioservicesService.getUsuarionome(this.Selnomeusuario);  
  }  
filterUsuarios(filterVal: any) {
  if (filterVal == "0")    
      this.usuarios = this.cacheUsuarios;      
  else
      this.usuarios = this.cacheUsuarios.filter((item) => item.nomeusuario == filterVal);
}

  ngOnInit() {
    this.show = false;
  
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 2,
      processing: true,
        language: {
          emptyTable: "Nenhum registro encontrado",
          info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          infoEmpty: "Mostrando 0 até 0 de 0 registros",
          infoFiltered: "(Filtrados de _MAX_ registros)",
          infoPostFix: "",
          lengthMenu: "_MENU_ resultados por página",
          loadingRecords: "Carregando...",
          processing: "Processando...",
          zeroRecords: "Nenhum registro encontrado",
          search: "Pesquisar",
          paginate: {
            next: "Próximo",
            previous: "Anterior",
            first: "Primeiro",
            last: "Último"
          },
          aria: {
            sortAscending: ": Ordenar colunas de forma ascendente",
            sortDescending: ": Ordenar colunas de forma descendente"
          }
        }
      };
      if (this.cacheUsuarios) {
        this.dtOptions.data =  this.cacheUsuarios
      }
  }
  

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
   
   
    const temp = this.rows.filter(x => 
      
      x.nomeusuario.toString().toLowerCase().includes(val.toString().toLowerCase())
      ||
      x.cpf.toString().toLowerCase().includes(val.toString().toLowerCase())
   
    )
    if(val && val.trim() != ''){
      this.rows = temp;
    }
    else{
      this.getdata()
      
    }
  }
  getdata(){
    return( this.UsuarioservicesService.getUsusarioList().subscribe(result => {
     
      this.usuarios = result;
      this.rows = result;
      this.collectionSize=result.length
      this.getpagina();
    }, error => console.error(error)))
  }
  getpagina(): Usuario[] {
     return this.rows
       .map((usu, i) => ({id: i + 1, ...usu}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
    

  }
  deleteUser(usuario: Usuario): void {
    localStorage.removeItem("editcodigoUsuario");
    localStorage.setItem("editcodigoUsuario", usuario.codigoUsuario.toString());
   //  this.produtoService.deleteUser(produtos.codigoProduto)
   //    .subscribe( data => {
   //      this.router.navigate(['ListProduto']);  
   //     // this.Colecao = this.Colecao.filter(u => u !== colecao);
   //    })
  };
  onUpdate(usuario: Usuario) {  
    localStorage.removeItem("editcodigoUsuario");
    localStorage.setItem("editcodigoUsuario", usuario.codigoUsuario.toString());
   //  this.produtoService.updateUser(this.ColecaoForm.value).subscribe(data => {  
   //    this.router.navigate(['ListColecao']);  
   //  },  
   //    error => {  
   //      alert(error);  
   //    });  
  }  
  edit(usuario: Usuario): void {
    console.log('edit'+usuario.codigoUsuario)
    localStorage.removeItem("editcodigoUsuario");
    localStorage.setItem("editcodigoUsuario",usuario.codigoUsuario.toString());
    const modalRef = this.modalService.open(EditUsuarioComponent);
  
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
    //this.router.navigate(['EditUsuario']);
    //this.router.navigate(['ListColecao']);  
  };
  View(usuario: Usuario): void {
    console.log('views'+usuario.codigoUsuario)
    localStorage.removeItem("editcodigoUsuario");
    localStorage.setItem("editcodigoUsuario", usuario.codigoUsuario.toString());
    const modalRef = this.modalService.open(DetailsUsuarioComponent);
  
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
    //this.router.navigate(['DetailsUsuario']);
    //this.router.navigate(['ListColecao']);  
  };

  search = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    map(term => term === '' ? []
      : this.rows.filter(v => v.nomeusuario.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
      
  )

formatter = (x: {usuario: Usuario}) => x.usuario;

openFormModal(usuario: Usuario) {
  const modalRef = this.modalService.open(EditUsuarioComponent);
  
  modalRef.result.then((result) => {
    console.log(result);
  }).catch((error) => {
    console.log(error);
  });
}
}


