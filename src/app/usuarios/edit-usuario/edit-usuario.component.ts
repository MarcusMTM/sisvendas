import { Component, OnInit,NgZone  } from '@angular/core';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioservicesService } from '../../services/usuarioservices.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-edit-usuario',
  templateUrl: './edit-usuario.component.html',
  styleUrls: ['./edit-usuario.component.scss']
})
export class EditUsuarioComponent implements OnInit {
  UsuarioForm: FormGroup;
  IssueArr: any = [];
  submitted = false;
  
  errors: any[] = [];
  //date: {year: number, month: number};
  mudancasNaoSalvas: boolean;
  constructor(
      public fb: FormBuilder,
      private ngZone: NgZone,
      private router: Router,
      public usuarioService: UsuarioservicesService,
      private modalService: NgbModal,
      private toastr: ToastrService,
      private spinner: NgxSpinnerService
    ) {}

ngOnInit() {

     this.addIssue()
    let empid = localStorage.getItem('editcodigoUsuario');  
    if (+empid > 0) {  
      this.usuarioService.getUsuarioById(+empid).subscribe(data => {       
        this.UsuarioForm.patchValue(data);  
      })  
      // this.btnvisibility = false;  
      // this.empformlabel = 'Edit Employee';  
      // this.empformbtn = 'Update';  
    } 
}
/* Conver date object to string */

addIssue() {
  this.UsuarioForm = this.fb.group({
    codigoUsuario:[''],
    nomeusuario: [{value: '', disabled: false}, Validators.required],
    login: [{value: '', disabled: false}, Validators.required],
    senha: [{value: '', disabled: false}, Validators.required],
     cpf: [{value: '', disabled: false},Validators.required],
     status:[{value: '', disabled: false},Validators.required]
});
}
submitForm() {
  
this.submitted = true;
 
  console.log(this.UsuarioForm.value)
    this.usuarioService.updateUsuario(this.UsuarioForm.value).subscribe(res => 
          
      sucesso => { this.processarSucesso(sucesso)  },
       falha => { this.processarFalha(falha) }
      
       // this.ngZone.run(() => this.router.navigateByUrl('/listUsuario'))
    );
   
    this.modalService.dismissAll();  
    

}

voltar() {
  localStorage.removeItem("editcodigoUsuario");
  this.modalService.dismissAll();
  this.router.navigateByUrl('/listUsuario');


}
processarFalha(fail: any) {
  this.errors = fail.error.errors;
  this.toastr.error('Ocorreu um erro!', 'Opa :(');
}

processarSucesso(response: any) {
  this.errors = [];    
  this.mudancasNaoSalvas = false;

  let toast = this.toastr.success('cadastrado com sucesso!', 'Sucesso!');
  if (toast) {

    toast.onHidden.subscribe(() => {  
      
      this.router.navigate(['listUsuario']);
      
    });
  }    
  this.UsuarioForm.reset();
  
}
}
