import { Component, OnInit, NgZone  } from '@angular/core';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ColecaoService } from '../../services/colecao.service';

import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-create-colecao',
  templateUrl: './create-colecao.component.html',
  styleUrls: ['./create-colecao.component.scss']
})
export class CreateColecaoComponent implements OnInit {
  ColecaoForm: FormGroup;
  IssueArr: any = [];
  submitted = false;
  errors: any[] = [];
  //date: {year: number, month: number};
  mudancasNaoSalvas: boolean;
 
  constructor(
      public fb: FormBuilder,
      private ngZone: NgZone,
      private router: Router,
      public colecaoservices: ColecaoService,
      private toastr: ToastrService,
      private spinner: NgxSpinnerService
      //public errorService: ErrorService
    ) {}

  ngOnInit() {
    console.log('Issue added!')
      this.addIssue()
      let empid = localStorage.getItem('editcolecaoId');  
      if (+empid > 0) {  
        this.colecaoservices.getUserById(+empid).subscribe(data => {  
          this.ColecaoForm.patchValue(data);  
        })  
        // this.btnvisibility = false;  
        // this.empformlabel = 'Edit Employee';  
        // this.empformbtn = 'Update';  
      } 
}
/* Conver date object to string */

  addIssue() {
      this.ColecaoForm = this.fb.group({
        descricao: ['',Validators.required],
        dataValidade:['',Validators.required]
      });
  }
  submitForm() {
    this.submitted = true;
    if (this.ColecaoForm.invalid) {
      this.errors =this.ColecaoForm.value;
      this.toastr.error('Ocorreu no cadastro!', JSON.stringify(this.ColecaoForm.value))
        return;
    }else{
      this.spinner.show();
      this.colecaoservices.createColecao(this.ColecaoForm.value).subscribe(
        sucesso => { this.processarSucesso(sucesso) },
        falha => { this.processarFalha(falha) }
       
         
          //this.ngZone.run(() => this.router.navigateByUrl('/ListColecao'))
      );
  }
}
voltar(): void {
  localStorage.removeItem("editcolecaoId");
 

  this.router.navigate(['ListColecao']);  
};
processarFalha(fail: any) {
  this.errors = fail.error.errors;
  this.toastr.error('Ocorreu um erro!', 'Opa :(');
}
processarSucesso(response: any) {
  this.errors = [];    
  this.mudancasNaoSalvas = false;
  //this.modalService.dismissAll();  
  let toast = this.toastr.success('cadastrado com sucesso!', 'Sucesso!');
  if (toast) {
    toast.onHidden.subscribe(() => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();      
    
      this.router.navigate(['ListColecao']);
      }, 1000);
      //this.router.navigate(['ListColecao']);
    });
  }
  this.ColecaoForm.reset();
  
}
}
