import { Component, OnInit,Inject,NgZone} from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Colecao } from '../colecao';
import { ColecaoService } from '../../services/colecao.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { EditColecaoComponent } from '../edit-colecao/edit-colecao.component';
import { DetailsComponent } from '../details/details.component';
import {debounceTime, map} from 'rxjs/operators';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-list-colecao',
  templateUrl: './list-colecao.component.html',
  styleUrls: ['./list-colecao.component.scss']
})
export class ListColecaoComponent implements OnInit {
  public Colecao: Colecao[];
  ColecaoForm: FormGroup;
  public httpteste: HttpClient;
  public url: string;
  public selectedName : string = "";
  rows:any;
  show: boolean = false;
  constructor(http: HttpClient,
    public ColecaoService: ColecaoService, 
     private ngZone: NgZone,
    private router: Router, 
     public fb: FormBuilder, private modalService: NgbModal) {
      

}

  ngOnInit() {
    this.getdata()
    this.show = false;
    this.ColecaoForm = this.fb.group({
      descricao: ['',Validators.required],
      dataValidade:['',Validators.required]
    });
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
   
   
    const temp = this.rows.filter(x => 
      
      x.descricao.toString().toLowerCase().includes(val.toString().toLowerCase())
   
    )
    if(val && val.trim() != ''){
      this.rows = temp;
    }
    else{
      this.getdata()
      
    }
  }
 
  delete(value){
   //s debugger
    this.selectedName = value;
    console.log("Add Delete Action code here" + value)
  }
  deleteUser(colecao: Colecao): void {
    localStorage.removeItem("editcolecaoId");
    localStorage.setItem("editcolecaoId", colecao.codigoColecao.toString());
    this.ColecaoService.deleteUser(colecao.codigoColecao)
      .subscribe( data => {
        this.router.navigate(['ListColecao']);  
       // this.Colecao = this.Colecao.filter(u => u !== colecao);
      })
  };
  onUpdate(colecao: Colecao) {  
    localStorage.removeItem("editcolecaoId");
    localStorage.setItem("editcolecaoId", colecao.codigoColecao.toString());
    this.ColecaoService.updateColecao(this.ColecaoForm.value).subscribe(data => {  
      this.router.navigate(['ListColecao']);  
    },  
      error => {  
        alert(error);  
      });  
  }  
  edit(colecao: Colecao): void {
    console.log('edit'+colecao.codigoColecao)
    localStorage.removeItem("editcolecaoId");
    localStorage.setItem("editcolecaoId", colecao.codigoColecao.toString());
    const modalRef = this.modalService.open(EditColecaoComponent);
  
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
    //this.router.navigate(['EditColecao']);
    //this.router.navigate(['ListColecao']);  
  };
  View(colecao: Colecao): void {''
    console.log('views'+colecao.codigoColecao)
    localStorage.removeItem("editcolecaoId");
    localStorage.setItem("editcolecaoId", colecao.codigoColecao.toString());
    const modalRef = this.modalService.open(DetailsComponent);
  
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
    //this.router.navigate(['DetalhesColecao']);
    //this.router.navigate(['ListColecao']);  
  };
  getdata(){
    return( this.ColecaoService.getColecaoList().subscribe(result => {
     
      this.Colecao = result;
      this.rows = result;
    }, error => console.error(error)))
  }
  search = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    map(term => term === '' ? []
      : this.rows.filter(v => v.descricao.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
      
  )

formatter = (x: {descricao: string}) => x.descricao;
}
