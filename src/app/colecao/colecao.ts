export interface Colecao {
    codigoColecao: number;
    descricao: string;
    dataValidade: Date;
}

