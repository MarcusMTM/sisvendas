import { Component, OnInit, NgZone  } from '@angular/core';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ColecaoService } from '../../services/colecao.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

 
  ColecaoForm: FormGroup;
  IssueArr: any = [];
  submitted = false;
  //date: {year: number, month: number};
 
  constructor(
      public fb: FormBuilder,
      private ngZone: NgZone,
      private router: Router,
      public colecaoservices: ColecaoService,
      private modalService: NgbModal
      //public errorService: ErrorService
    ) {}

  ngOnInit() {
 
       this.addIssue()
      let empid = localStorage.getItem('editcolecaoId');  
      if (+empid > 0) {  
        this.colecaoservices.getUserById(+empid).subscribe(data => {  
          console.log('data'+data)
          this.ColecaoForm.patchValue(data);  
        })  
        // this.btnvisibility = false;  
        // this.empformlabel = 'Edit Employee';  
        // this.empformbtn = 'Update';  
      } 
}
/* Conver date object to string */

  addIssue() {
       this.ColecaoForm = this.fb.group({
         descricao: [{value: '', disabled: true},Validators.required],
         dataValidade:[{value: '', disabled: true},Validators.required]
       });
  }
  submitForm() {
    this.modalService.dismissAll();
    this.router.navigateByUrl('ListColecao');
    // if (this.ColecaoForm.invalid) {
    //   alert('ERR0!! :-)\n\n' + JSON.stringify(this.ColecaoForm.value))
    //     return;
    // }else{
      
    //   this.colecaoservices.createColecao(this.ColecaoForm.value).subscribe(res => {
    //      console.log('Issue added!')
         
    //       this.ngZone.run(() => this.router.navigateByUrl('/ListColecao'))
    //   });
  
}

}
