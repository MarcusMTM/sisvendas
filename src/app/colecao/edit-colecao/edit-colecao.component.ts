import { Component, OnInit, NgZone  } from '@angular/core';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ColecaoService } from '../../services/colecao.service';
import {formatDate } from '@angular/common';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";
import { DatePipe,CurrencyPipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-edit-colecao',
  templateUrl: './edit-colecao.component.html',
  styleUrls: ['./edit-colecao.component.scss']
})

export class EditColecaoComponent implements OnInit {
  pipe = new DatePipe('en-US');
  ColecaoForm: FormGroup;
  IssueArr: any = [];
  submitted = false;
  errors: any[] = [];
  //date: {year: number, month: number};
  mudancasNaoSalvas: boolean;
 
  constructor(
      public fb: FormBuilder,
      private ngZone: NgZone,
      private router: Router,
      public colecaoservices: ColecaoService,
      private modalService: NgbModal,
      private toastr: ToastrService,
      private spinner: NgxSpinnerService
    ) {}

  ngOnInit() {
    const currentDate = new Date().toISOString().substring(0, 10);
      this.addIssue()
      let empid = localStorage.getItem('editcolecaoId');  
      if (+empid > 0) {  
        this.colecaoservices.getUserById(+empid).subscribe(data => {  
          //  this.ColecaoForm.setValue({
          //   dataValidade:formatDate(data.dataValidade.toDateString(), 'yyyy-MM-dd', 'en-US'),
          //    descricao:data.descricao.toString()});
          
          this.ColecaoForm.patchValue({codigoColecao: data.codigoColecao, 
            dataValidade:this.pipe.transform(data.dataValidade,'yyyy-MM-dd'),
          descricao:data.descricao}); 
          
         // this.ColecaoForm.patchValue(data)
        })  
        // this.btnvisibility = false;  
        // this.empformlabel = 'Edit Employee';  
        // this.empformbtn = 'Update';  
      } 
}
/* Conver date object to string */
currentDate() {
  const currentDate = new Date();
  return currentDate.toISOString().substring(0,10);
}
  addIssue() {
      this.ColecaoForm = this.fb.group({
        codigoColecao:[''],
        descricao: [{value: ''},Validators.required],
        dataValidade:[{value: ''},Validators.required]
      });
  }
  submitForm() {
    this.submitted = true;
    //this.router.navigateByUrl('/ListColecao');
    if (this.ColecaoForm.invalid) {
      alert('ERR0!! :-)\n\n' + JSON.stringify(this.ColecaoForm.value))
        return;
    }else{
      this.spinner.show();
      this.colecaoservices.updateColecao(this.ColecaoForm.value).subscribe(
         sucesso => { this.processarSucesso(sucesso) },
      falha => { this.processarFalha(falha) });
  
  }
  }
  voltar(): void {
    this.modalService.dismissAll();
    localStorage.removeItem("editcolecaoId");
   
  
    this.router.navigate(['ListColecao']);  
  };
  processarFalha(fail: any) {
    this.errors = fail.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Opa :(');
  }
  processarSucesso(response: any) {
    this.errors = [];    
    this.mudancasNaoSalvas = false;
    this.modalService.dismissAll();  
    let toast = this.toastr.success('cadastrado com sucesso!', 'Sucesso!');
    if (toast) {
  
      toast.onHidden.subscribe(() => {  
        setTimeout(() => {       
          this.spinner.hide(); 
        this.router.navigate(['ListColecao']);
        }, 4000);
      });
    }    
    this.ColecaoForm.reset();
    
  }
}
