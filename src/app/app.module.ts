import { BrowserModule } from '@angular/platform-browser';
import { NgModule  } from '@angular/core';
import { AngularMaterialModule } from './angular-material.module';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CreateUsuarioComponent } from './usuarios/create-usuario/create-usuario.component';
import { ListUsuarioComponent } from './usuarios/list-usuario/list-usuario.component';
import { DetailsUsuarioComponent } from './usuarios/details-usuario/details-usuario.component';
import { DeleteUsuarioComponent } from './usuarios/delete-usuario/delete-usuario.component';
import { ToastrModule } from 'ngx-toastr';

import { HeaderComponent } from './navigation/header/header.component';

import { ClientesModule } from './cliente/clientes.module';
import { CreateProdutoComponent } from './produto/create-produto/create-produto.component';
import { ListTamanhoComponent } from './tamanho/list-tamanho/list-tamanho.component';
import { CreateTamanhoComponent } from './tamanho/create-tamanho/create-tamanho.component';
import { EditTamanhoComponent } from './tamanho/edit-tamanho/edit-tamanho.component';
import { DeleteTamanhoComponent } from './tamanho/delete-tamanho/delete-tamanho.component';
import { ListColecaoComponent } from './colecao/list-colecao/list-colecao.component';
import { DeleteColecaoComponent } from './colecao/delete-colecao/delete-colecao.component';
import { EditColecaoComponent } from './colecao/edit-colecao/edit-colecao.component';
import { CreateColecaoComponent } from './colecao/create-colecao/create-colecao.component';
import { CreateClienteComponent } from './cliente/create-cliente/create-cliente.component';
import { EditClienteComponent } from './cliente/edit-cliente/edit-cliente.component';
import { DeleteClienteComponent } from './cliente/delete-cliente/delete-cliente.component';
import { ListClienteComponent } from './cliente/list-cliente/list-cliente.component';
import { ListPedidoComponent } from './pedido/list-pedido/list-pedido.component';
import { CreatePedidoComponent } from './pedido/create-pedido/create-pedido.component';
import { DeletePedidoComponent } from './pedido/delete-pedido/delete-pedido.component';
import { EditPedidoComponent } from './pedido/edit-pedido/edit-pedido.component';
import { EditProdutoComponent } from './produto/edit-produto/edit-produto.component';
import { ListProdutoComponent } from './produto/list-produto/list-produto.component';
import { DeleteProdutoComponent } from './produto/delete-produto/delete-produto.component';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';

import { UsuarioservicesService } from './services/usuarioservices.service';
import { ClienteService } from './services/cliente.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PedidoService } from './services/pedido.service';
import { CreatePedidoitemComponent } from './pedidoitem/create-pedidoitem/create-pedidoitem.component';
import { EditPedidoitemComponent } from './pedidoitem/edit-pedidoitem/edit-pedidoitem.component';
import { ListPedidoitemComponent } from './pedidoitem/list-pedidoitem/list-pedidoitem.component';
import { DetailsPedidoitemComponent } from './pedidoitem/details-pedidoitem/details-pedidoitem.component';
import { DeletePedidoitemComponent } from './pedidoitem/delete-pedidoitem/delete-pedidoitem.component';
import { ModalProdutoComponent } from './modal/modal-produto/modal-produto.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AgGridModule } from 'ag-grid-angular';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import { MenuComponent } from './navigation/menu/menu.component';
import { LoginComponent } from './usuarios/login/login.component';
import { MenuUserComponent } from './usuarios/menu-user/menu-user.component';
import { UserService } from './usuarios/userService';
import { DetailsComponent } from './colecao/details/details.component';
import { EditUsuarioComponent } from './usuarios/edit-usuario/edit-usuario.component';
import { DetailTamanhoComponent } from './tamanho/detail-tamanho/detail-tamanho.component';
import { DetailProdutoComponent } from './produto/detail-produto/detail-produto.component';
import { DetailPedidoComponent } from './pedido/detail-pedido/detail-pedido.component';
import { DetailClienteComponent } from './cliente/detail-cliente/detail-cliente.component';
import { PedidoitemGestaoComponent } from './pedido/pedidoitem-gestao/pedidoitem-gestao.component';
import { ListaclienteComponent } from './pedido/listacliente/listacliente.component';
import { ProdutoModule } from './produto/produto.module';
import { TextMaskModule } from 'angular2-text-mask';
import { NgBrazil,TextMask } from 'ng-brazil' 

import { ErrorInterceptor } from './services/error.handler.service';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { NgxSpinnerModule } from "ngx-spinner";
export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
];

@NgModule({
  declarations: [
    AppComponent,
    CreateUsuarioComponent,
    ListUsuarioComponent,
    DetailsUsuarioComponent,
    DeleteUsuarioComponent,
    HeaderComponent,
    CreateProdutoComponent,
    ListTamanhoComponent,
    CreateTamanhoComponent,
    EditTamanhoComponent,
    DeleteTamanhoComponent,
    ListColecaoComponent,
    DeleteColecaoComponent,
    EditColecaoComponent,
    CreateColecaoComponent,
    CreateClienteComponent,
    EditClienteComponent,
    DeleteClienteComponent,
    ListClienteComponent,
    ListPedidoComponent,
    CreatePedidoComponent,
    DeletePedidoComponent,
    EditPedidoComponent,
    EditProdutoComponent,
    ListProdutoComponent,
    DeleteProdutoComponent,
    CreatePedidoitemComponent,
    EditPedidoitemComponent,
    ListPedidoitemComponent,
    DetailsPedidoitemComponent,
    DeletePedidoitemComponent,
    ModalProdutoComponent,
    MenuComponent,
    LoginComponent,
    MenuUserComponent,
    DetailsComponent,
    EditUsuarioComponent,
    DetailTamanhoComponent,
    DetailProdutoComponent,
    DetailPedidoComponent,
    DetailClienteComponent,
    PedidoitemGestaoComponent,
    ListaclienteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    ClientesModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    NgxDatatableModule,
    AgGridModule,
    NgbModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),// ToastrModule added,
    TextMaskModule,
    NgBrazil,
    CurrencyMaskModule,
    NgxSpinnerModule
   
     ],
  providers: [UsuarioservicesService,ClienteService,PedidoService,UserService,
    httpInterceptorProviders,
    ],
  bootstrap: [AppComponent],
  entryComponents: []
})
export class AppModule { }
