import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateUsuarioComponent } from './usuarios/create-usuario/create-usuario.component';
import { ListUsuarioComponent } from './usuarios/list-usuario/list-usuario.component';
import { DetailsUsuarioComponent } from './usuarios/details-usuario/details-usuario.component';
import { DeleteUsuarioComponent } from './usuarios/delete-usuario/delete-usuario.component';
import { EditUsuarioComponent } from './usuarios/edit-usuario/edit-usuario.component';
import { ListPedidoComponent } from './pedido/list-pedido/list-pedido.component';
import { CreatePedidoComponent } from './pedido/create-pedido/create-pedido.component';
import { DeletePedidoComponent } from './pedido/delete-pedido/delete-pedido.component';
import { EditPedidoComponent } from './pedido/edit-pedido/edit-pedido.component';
import { ListTamanhoComponent } from './tamanho/list-tamanho/list-tamanho.component';
import { CreateTamanhoComponent } from './tamanho/create-tamanho/create-tamanho.component';
import { EditTamanhoComponent } from './tamanho/edit-tamanho/edit-tamanho.component';
import { DetailTamanhoComponent } from './tamanho/detail-tamanho/detail-tamanho.component';
import { DeleteTamanhoComponent } from './tamanho/delete-tamanho/delete-tamanho.component';
import { ListColecaoComponent } from './colecao/list-colecao/list-colecao.component';
import { DeleteColecaoComponent } from './colecao/delete-colecao/delete-colecao.component';
import { EditColecaoComponent } from './colecao/edit-colecao/edit-colecao.component';
import { CreateColecaoComponent } from './colecao/create-colecao/create-colecao.component';
import { CreateClienteComponent } from './cliente/create-cliente/create-cliente.component';
import { EditClienteComponent } from './cliente/edit-cliente/edit-cliente.component';
import { DeleteClienteComponent } from './cliente/delete-cliente/delete-cliente.component';
import { ListClienteComponent } from './cliente/list-cliente/list-cliente.component';
import { EditProdutoComponent } from './produto/edit-produto/edit-produto.component';
import { ListProdutoComponent } from './produto/list-produto/list-produto.component';
import { DeleteProdutoComponent } from './produto/delete-produto/delete-produto.component';
import { CreateProdutoComponent } from './produto/create-produto/create-produto.component';
import { CreatePedidoitemComponent } from './pedidoitem/create-pedidoitem/create-pedidoitem.component';
import { EditPedidoitemComponent } from './pedidoitem/edit-pedidoitem/edit-pedidoitem.component';
import { ListPedidoitemComponent } from './pedidoitem/list-pedidoitem/list-pedidoitem.component';
import { DetailsPedidoitemComponent } from './pedidoitem/details-pedidoitem/details-pedidoitem.component';
import { DeletePedidoitemComponent } from './pedidoitem/delete-pedidoitem/delete-pedidoitem.component';
import { ModalProdutoComponent } from './modal/modal-produto/modal-produto.component';
import { HomeComponent } from './navigation/home/home.component';
import { LoginComponent } from './usuarios/login/login.component';
import { DetailsComponent } from './colecao/details/details.component';
import { DetailClienteComponent } from './cliente/detail-cliente/detail-cliente.component';
import { DetailProdutoComponent } from './produto/detail-produto/detail-produto.component';
import { PedidoitemGestaoComponent } from './pedido/pedidoitem-gestao/pedidoitem-gestao.component';
import { ListaclienteComponent } from './pedido/listacliente/listacliente.component';
const routes: Routes = [
  { path: 'listUsuario', component: ListUsuarioComponent},  { path: 'CreateUsuario', component: CreateUsuarioComponent},
  { path: 'DetailsUsuario', component: DetailsUsuarioComponent},  { path: 'DeleteUsuario', component: DeleteUsuarioComponent},
  { path: 'EditUsuario', component: EditUsuarioComponent},
  { path: 'ListPedidoComponent', component: ListPedidoComponent},  { path: 'cadastroPedido', component: CreatePedidoComponent},
  { path: 'DeletePedido', component: DeletePedidoComponent},  { path: 'EditPedido', component: EditPedidoComponent},
  { path: 'ListTamanho', component: ListTamanhoComponent},  { path: 'CreateTamanho', component: CreateTamanhoComponent},
  { path: 'EditTamanho', component: EditTamanhoComponent},  { path: 'DeleteTamanho', component: DeleteTamanhoComponent},
  {path:'DetailTamanho',component:DetailTamanhoComponent},
  { path: 'ListColecao', component: ListColecaoComponent},  { path: 'DeleteColecao', component: DeleteColecaoComponent},
  { path: 'EditColecao', component: EditColecaoComponent},  { path: 'CreateColecao', component: CreateColecaoComponent},
  { path: 'DetalhesColecao', component: DetailsComponent},
  { path: 'CreateCliente', component: CreateClienteComponent},  { path: 'EditCliente', component: EditClienteComponent},
  { path: 'DeleteCliente', component: DeleteClienteComponent},  { path: 'ListCliente', component: ListClienteComponent},
  {path:'DetailCliente',component:DetailClienteComponent},
  { path: 'EditProduto', component: EditProdutoComponent},  { path: 'ListProduto', component: ListProdutoComponent},
  { path: 'DeleteProduto', component: DeleteProdutoComponent},  { path: 'CreateProduto', component: CreateProdutoComponent},
  {path:'DetailProdutos',component:DetailProdutoComponent},
  { path: 'EditPedidoItem', component: EditPedidoitemComponent},  { path: 'ListPedidoitem', component: ListPedidoitemComponent},
  { path: 'DetailsPedidoitem', component: DetailsPedidoitemComponent},  { path: 'CreatePedidoItem', component: CreatePedidoitemComponent},
  { path: 'DeletePedidoItem', component: DeletePedidoitemComponent}, { path: 'PedidoGestao', component: PedidoitemGestaoComponent},
  { path: 'HomeComponent', component: HomeComponent},
  { path: 'ListaClientePedido', component: ListaclienteComponent},
  
  {
    path: 'produtos',
    loadChildren: () => import('.//produto/produto.module')
      .then(m => m.ProdutoModule)
  },
  { path: 'modal', component: ModalProdutoComponent},
  { path: 'HomeComponent', redirectTo: 'home', pathMatch: 'full' },
  {path: 'entrar', component: LoginComponent }  
  //{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
