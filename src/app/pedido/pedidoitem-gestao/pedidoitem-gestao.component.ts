import { Component, OnInit,Inject,NgZone } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { pedidoitem } from '../../pedidoitem/pedidoItem';
import { PedidoitemService } from '../../services/pedidoitem.service';
import { Observable } from 'rxjs';
import { PedidoService } from '../../services/pedido.service';
import {debounceTime, map,distinctUntilChanged} from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { DatePipe } from '@angular/common';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { EditPedidoitemComponent } from '../../pedidoitem/edit-pedidoitem/edit-pedidoitem.component';

@Component({
  selector: 'app-pedidoitem-gestao',
  templateUrl: './pedidoitem-gestao.component.html',
  styleUrls: ['./pedidoitem-gestao.component.scss']
})
export class PedidoitemGestaoComponent implements OnInit {
  pedidoitem: pedidoitem[];
  rows : any;
  resumo:any;
  GestaoForm: FormGroup;
  PedidoForm: FormGroup;
  ColecaoForm:FormGroup;
  Pedidoitem$: Observable<pedidoitem[]>;
  show: boolean = false;
  columnsWithSearch : string[] = [];
  selected = [];
  quantidade=0;
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  constructor(http: HttpClient, 
    public PedidoitemService: PedidoitemService,  
    private modalService: NgbModal,
    public fb: FormBuilder, 
    private ngZone: NgZone,
    private router: Router,
    public pedidoService: PedidoService,
   ) {
    
    //this.Pedidoitem$= this.PedidoitemService.getPedidosList();
    //this.PedidoitemService.getPedidosList();
    //this.getdata()
    this.iniciarForm();
    this.addIssue();

}

  ngOnInit() {
    let pipe = new DatePipe('en-US');
   
    //pipe.transform(now, 'short');
    const currentDate = new Date().toISOString().substring(0, 10);
  
    let empid = localStorage.getItem('editcodigoPedido'); 
    console.log('empid'+empid)
   // this.updateFilter(empid);
    if (+empid > 0) {  
       this.pedidoService.getPedidosById(+empid).subscribe(data =>{

       });
      
      this.pedidoService.getPedidosResumo(+empid).subscribe(data => {  
     
       this.GestaoForm.patchValue(
         {
          dataPedido:pipe.transform(data.dataPedido, 'dd/MM/yyyy'),
          codigoPedido:data.cliente.nome,
          formapagamentoCodigo:data.formapagamento.descricao,
          valor:data.valor,
          status:data.status
        });    
      
      })  
      this.PedidoitemService.getPedidosId(+empid).subscribe(data => {  
        this.selected = [data[2]];
       this.rows=data;
      })
      this.PedidoitemService.getPedidoItemResumo(+empid).subscribe(data => {  
        this.quantidade=data.quantidadeItem
       
        this.GestaoForm.patchValue(
          {
            quantidadeItem:data.quantidadeItem
          }); 
      })   
      
      
    }
  }
  addIssue() {
    this.GestaoForm = this.fb.group({
      dataPedido: [{value: '', disabled: true},Validators.required],
      formapagamentoCodigo: [{value: '', disabled: true},Validators.required],
      codigoCliente: [{value: '', disabled: true},Validators.required],
      codigoPedido: [{value: '', disabled: true},Validators.required],
      codigoProduto: [{value: '', disabled: true},],
      quantidadeItem:[{value: '', disabled: true},],
      valor:[{value: '', disabled: true},],
      status:[{value: '', disabled: true},]
      
    });
   
}
onActivate(event) {
  if(event.type == 'click') {

      this.selected.forEach(novoproduto => {
        console.log(novoproduto.codigoPedido)
    })
     
  }
}
onSelect({ selected }) {

  this.selected.forEach(item => {
      console.log(item)
  localStorage.removeItem("editcodigoPedidoitem");
  localStorage.setItem("editcodigoPedidoitem",item.codigoPedidoitem.toString());
  const modalRef = this.modalService.open(EditPedidoitemComponent,  { size: 'lg', backdrop: 'static' });

  // modalRef.result.then((result) => {
  //   console.log(result);
  // }).catch((error) => {
  //   console.log(error);
  // });

  })
  console.log(this.selected)
}

onKey(event: any) { 
}
  iniciarForm() {
    

    
    this.PedidoForm = this.fb.group({
    
      dataPedido: ['',Validators.required],
      formapagamentoCodigo: ['',Validators.required],
      codigoCliente: ['',Validators.required],
      valor: [''],
      status:true,
      //dataPedidoITEM:[''],
      codigoProduto:[''],
      codigoPedido: 0,
      quantidadeItem:[''],
      valoritem:['']
      //statusITEM:true
    })
  }
  getdata(){
    return( this.PedidoitemService.getPedidosList().subscribe(result => {
     
        this.pedidoitem = result;
       // this.rows = result;
      }, error => console.error(error)))
  }
  setValue(){
    this.GestaoForm.setValue({codigoPedido: 1, quantidadeItem: 1, 
    codigoProduto: 1});
}
submitForm() {

  this.modalService.dismissAll();
  
  this.ngZone.run(() => this.router.navigateByUrl('/ListPedidoComponent'))
  
}
updateFilter(event: any) {
  const val = event.target.value.toLowerCase();
   
  console.log('event');
  console.log(val);
  const temp = this.rows.filter(x => 
  
    x.codigoPedido.toString().toLowerCase().includes(val.toString().toLowerCase())
   
  )
  console.log('temp')
  console.log(temp)
  if(event && event.toString().trim() != ''){
    this.rows = temp;
  }
  else{
    this.getdata()
    
  }
}
voltar()
{

}
}
