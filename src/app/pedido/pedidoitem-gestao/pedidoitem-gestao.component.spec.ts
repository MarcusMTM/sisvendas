import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidoitemGestaoComponent } from './pedidoitem-gestao.component';

describe('PedidoitemGestaoComponent', () => {
  let component: PedidoitemGestaoComponent;
  let fixture: ComponentFixture<PedidoitemGestaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidoitemGestaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidoitemGestaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
