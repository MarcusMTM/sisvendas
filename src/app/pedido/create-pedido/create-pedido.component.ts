import { Component, OnInit, NgZone,Input ,OnChanges,ViewChild,ElementRef  } from '@angular/core';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PedidoService } from '../../services/pedido.service';
import { PedidoitemService } from '../../services/pedidoitem.service';
import { FormadepagamentoService } from '../../services/formadepagamento.service';
import { Cliente } from '../../cliente/cliente';
import { Produtos } from '../../produto/Produtos';
import { formapagamento } from '../../pedido/formapagamento';
import { Observable } from 'rxjs';
import { ClienteService } from '../../services/cliente.service';
import { ProdutoService } from '../../services/produto.service';
import {FormControl} from '@angular/forms';
import {map, startWith, debounceTime, distinctUntilChanged, switchMap, catchError} from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { pedidoitem } from '../../pedidoitem/pedidoItem';
import { ToastrService } from 'ngx-toastr';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {ListaclienteComponent} from '../listacliente/listacliente.component'
import { MASKS, NgBrazilValidators,TextMask } from 'ng-brazil';
import { StringUtils } from 'src/app/utils/string-utils';
import { ProdutosSelecionado } from '../ProdutoSelecionado';
import { CurrencyMaskConfig, CurrencyMaskModule, CURRENCY_MASK_CONFIG } from 'ng2-currency-mask';

import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-create-pedido',
  templateUrl: './create-pedido.component.html',
  styleUrls: ['./create-pedido.component.scss']
})


export class CreatePedidoComponent implements OnInit {
  inputValue = "";
  quantidade = 0;
  valortotal=0;
  PedidoForm: FormGroup;
  pedidoItemForm: FormGroup;
  classgeneric: any = [];
  cliente$: Observable<Cliente[]>;
  produto$: Observable<Produtos[]>;
  formapagamento$: Observable<formapagamento[]>;
  pedidoitem$: Observable<pedidoitem[]>;
  myControl = new FormControl();
  options: Cliente[];
  optionsformapagamentos: formapagamento[];

  filteredOptions: Observable<Cliente[]>;
  filteredOptionsProdutos: Observable<Produtos[]>;
  filteredOptionsformapagamento:Observable<formapagamento[]>;
  ClientesSubscription: Subscription;
  ProdutosSubscription: Subscription;
  formapagamentosSubscription: Subscription;
  public pedidoitem:pedidoitem;
  public pedidoItemDetails;
   submitted = false;
  rows = [];
  columns = []
  selected = [];
  ListaProdutosSelecionado: ProdutosSelecionado[];
  ResultProduto: ProdutosSelecionado[];
  ResultProdutoFinal: [];
  tempProdutosSelecionado:ProdutosSelecionado;
  errors: any[] = [];
  //date: {year: number, month: number};
  mudancasNaoSalvas: boolean;
  SelecionadoCodigoCliente:Int16Array;
  public MASKS = MASKS;
  selecionado=[];
  selecionado2=[];
  listdesconto: any=[0,5,10,20,50];
  constructor(

    public fb: FormBuilder,
    private ngZone: NgZone,
    private router: Router,
    public pedidoService: PedidoService,
    public clienteService: ClienteService,
    public produtoService:ProdutoService,
    public PedidoitemService:PedidoitemService,
    public FormadepagamentoService:FormadepagamentoService,
    private toastr: ToastrService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService
  ) { 
  
    this.myControl = new FormControl();
    this.columns = [
      {
        prop: 'selected',
        name: '',
        sortable: false,
        canAutoResize: false,
        draggable: false,
        resizable: false,
        headerCheckboxable: true,
        checkboxable: true,
        width: 30
      },
      { prop: 'descricao' },
      { prop: 'valor' },

    ]
    
  }
  
onSelect(row) {
    console.log('row')
    console.log(row)
    this.PedidoForm.patchValue(
      {
        quantidadeItem: 0,
        ValorTotalItem:0
      }
    )
    
}
ngOnInit() {
   
   this.quantidade=0
    this.addIssue()
    this.getcliente()
    this.getProdutos()
    this.getFormapagamento()
    
  
    this.ClientesSubscription = this.clienteService.getClienteList().subscribe((clientes: Cliente[]) => {

    this.options = clientes;
    this.filteredOptions = this.myControl.valueChanges
        .pipe(
        startWith(''),
        map(options => options ? this._filter(options) : this.options.slice())
          );
  }); 

}   
transform(value: any, args?: any): any {
    return new Intl.NumberFormat('pt-BR', {style: 'currency', currency: 'BRL'}).format(value);
}
onKey(event: any,TESTE:number) { // without type info
      console.log('TESTE')
      console.log(TESTE)
      let tempvalor=0;
      this.quantidade=0;
      this.valortotal=0
      let cont=this.selected.length
      this.quantidade = parseInt(event.target.value);     
      this.selecionado= this.selected.filter(x =>
        x.codigoProduto==(TESTE)
       
        ); 

      this.selecionado.forEach(novoproduto => {    
        this.valortotal+=novoproduto.valor*this.quantidade;        
      });
      let exampleArray : any;
      exampleArray= {
              Quantidade:this.quantidade ,
              codigoProduto:TESTE,
              valor: this.valortotal
             
             
            } ;
      //this.selecionado2 = this.selecionado.filter(item => item.codigoProduto !== TESTE );
   
      console.log('exampleArray')
      console.log(exampleArray)
      this.selecionado2.push(exampleArray)
      console.log('selecionado2')
      console.log(this.selecionado2)
      
      console.log('valor to '+this.valortotal)
    
}
 
removeItem(event: any){

    this.selected= this.selected.filter(x =>
      x.codigoProduto!=(event));  

      this.selecionado2=this.selecionado2.filter(x =>
        x.codigoProduto!=(event)); 

      let valorfinal=0
      let ValorTotalItem=0
      this.selecionado2.forEach(novoproduto => {
        ValorTotalItem=novoproduto.valor
        valorfinal+=novoproduto.valor;
        
      });
      this.PedidoForm.patchValue(
        {
          valor: valorfinal
        })
}
changeDesconto(){
      let valorcalculado=0.00
      let vardesconto=this.PedidoForm.controls['Desconto'].value;
      
     
      let valorfinal=0
      let ValorTotalItem=0
      this.selecionado2.forEach(novoproduto => {
        ValorTotalItem=novoproduto.valor
        valorfinal+=novoproduto.valor;
        
      });    


      valorcalculado=valorfinal
    
      console.log(valorcalculado*(vardesconto/100))
      if(vardesconto>0){
        this.valortotal=(valorcalculado-(valorcalculado*(vardesconto/100)));
      }
      else{
        this.valortotal=valorfinal
      }
   
      this.PedidoForm.patchValue(
        {
          valor: this.valortotal
        })
       
}
AddItems(){
    
      let valorfinal=0
      let ValorTotalItem=0
      this.selecionado2.forEach(novoproduto => {
        ValorTotalItem=novoproduto.valor
        valorfinal+=novoproduto.valor;
        
      });
       this.PedidoForm.patchValue(
        {
          ValorTotalItem:ValorTotalItem,
          valor: valorfinal
        }
      )
    
}
   
addIssue() {
    this.PedidoForm = this.fb.group({
    
      dataPedido: ['',Validators.required],
      formapagamentoCodigo: ['',Validators.required],
      codigoCliente: ['',Validators.required],
      Cliente: ['',Validators.required],
      valor: ['', [<any>Validators.required, <any>NgBrazilValidators.currency]],
      status:true,
      //dataPedidoITEM:[''],
      codigoProduto:[''],
      codigoPedido: 0,
      quantidadeItem: ['',Validators.required],
      valoritem:['', [<any>Validators.required, <any>NgBrazilValidators.currency]],
      ValorTotalItem:[''],
      Desconto:['']

    })

      this.pedidoItemForm = this.fb.group({
    
       dataPedido:[''],
       codigoProduto:[''],
       codigoPedido: [''],
       quantidadeItem:[''],
       valoritem:[''],
       status:true
     })
    
}
getFormattedPrice(price: number) {
  return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(price);
}
submitForm() {
  let count= this.selected.length
  let pedidoaux =0;
  let arrayproduto:any=[];
  const firstElement = this.selected.shift();

  let valuesCodigoCliente=this.PedidoForm.controls['codigoCliente'].value
  let SelecionadoCodigoCliente = localStorage.getItem('SelecionadoCodigoCliente');
 
const  codigocliente= parseInt(valuesCodigoCliente)
let valorproduto2=parseInt(this.PedidoForm.controls['valor'].value)
//const  valorteste= parseInt(valorproduto)
console.log('valorproduto2')
console.log(valorproduto2)

  this.PedidoForm.patchValue({
    codigoProduto: firstElement.codigoProduto, 
    valoritem: firstElement.valor,
    valor: valorproduto2 
  });
  this.PedidoForm.patchValue({codigocliente:codigocliente});
  this.submitted = true;
  console.log(this.PedidoForm.value)
  this.pedidoService.createPedidos(this.PedidoForm.value).subscribe(res => {
    this.spinner.show();
    pedidoaux=res.codigoPedido;
    this.PedidoForm.patchValue({codigoPedido:res.codigoPedido});
      if (res.codigoPedido !== undefined) {
       
        this.selected.forEach(novoproduto => {
     
          this.PedidoForm.patchValue({codigoPedido:res.codigoPedido});
          this.PedidoForm.patchValue({codigoProduto: novoproduto.codigoProduto, valoritem: novoproduto.valor,valor: valorproduto2});
          this.pedidoService.createPedidos(this.PedidoForm.value).subscribe(res=>{

            console.log('cadastrado')
          }
          
                    
            
          )
        });
      
      }
    });
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();      
  
      this.router.navigate(['ListPedidoComponent']);
    }, 1000);
    //this.ngZone.run(() => this.router.navigateByUrl('/ListPedidoComponent'))
    
}
getcliente(){
   
  this.cliente$= this.clienteService.getClienteList();

 }

getFormapagamento(){
   
  this.formapagamento$= this.FormadepagamentoService.getformapagamentoList();
}

 getProdutos(){
   
  this.produtoService.getProdutoSelecionado().subscribe((ProdutosSele: ProdutosSelecionado[]) => {
    this.ListaProdutosSelecionado=ProdutosSele;
    console.log('this.ListaProdutosSelecionado teste')
    console.log(this.ListaProdutosSelecionado)
  })
  this.ProdutosSubscription= this.produtoService.getProdutosList().subscribe((produtos: Produtos[]) => {
  
    this.classgeneric = produtos;   
    this.rows = produtos;
    this.filteredOptionsProdutos = this.PedidoForm.valueChanges
        .pipe(
        startWith(''),
        map(classgeneric => classgeneric ? this._filter(classgeneric) : this.classgeneric.slice())
        );
});   


 }

_filter(value: string): Cliente[] {
  const filterValue = value.toLowerCase();
  return this.options.filter(x =>
    x.nome.toUpperCase().indexOf(filterValue.toUpperCase()) !== -1);
  
}
ProdutoFilter(event) {
  const val = event.target.value.toLowerCase();
 
 
  const temp = this.rows.filter(x => 
    
    x.descricao.toString().toLowerCase().includes(val.toString().toLowerCase())
  
 
  )
  if(val && val.trim() != ''){
    this.rows = temp;
  }
  else{
    this.getProdutos()
    
  }
}

add() {
  this.selected.push(this.rows[1], this.rows[3]);
  
  }
 
update() {
  this.selected = [this.rows[1], this.rows[3]];
}

remove() {
  this.selected = [];
  this.valortotal=0;
}
search = (text$: Observable<string>) =>


  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term === '' ? []
      : this.options.filter(v =>  v.nome.toLowerCase().toString().indexOf(term.toLowerCase()) > -1 ||
      v.codigoCliente.toString().indexOf(term.toLowerCase()) > -1
      ).slice(0, 10)
      
      )
      
  )
  formatter = (result: Cliente) => result.codigoCliente+":"+ result.nome;
//formatter = (x: {result: Cliente}) => x.result.codigoCliente+ x.result.nome;
public model: Cliente;
searchcliente = (text$: Observable<string>) =>
text$.pipe(
  debounceTime(200),
  map(term => term === '' ? []
    : this.options.filter(v => v.nome.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    
)

formatter3 = (result: Cliente) => result.codigoCliente+":"+ result.nome;
    
processarFalha(fail: any) {
  this.errors = fail.error.errors;
  this.toastr.error('Ocorreu um erro!', 'Opa :(');
}
processarSucesso() {
  this.errors = [];    
  this.mudancasNaoSalvas = false;
  console.log('teste sucesso')
  let toast = this.toastr.success('cadastrado com sucesso!', 'Sucesso!');
  if (toast) {
    toast.onHidden.subscribe(() => {
      this.router.navigate(['ListPedidoComponent']);
      this.PedidoForm.reset();
    });
  }
    
}
View() { 
  
  const modalRef = this.modalService.open(ListaclienteComponent, { size: 'lg' , centered: true  });

    modalRef.result.then((result) => {
      console.log(result);
      let SelecionadoCliente = localStorage.getItem('SelecionadoCliente');
      let SelecionadoCodigoCliente= localStorage.getItem("SelecionadoCodigoCliente");
      console.log('SelecionadoCliente') 
      this.PedidoForm.patchValue({Cliente:SelecionadoCliente});
      this.PedidoForm.patchValue({codigoCliente:SelecionadoCodigoCliente});
    }).catch((error) => {
      console.log('error');
      let SelecionadoCliente = localStorage.getItem('SelecionadoCliente');
      let SelecionadoCodigoCliente= localStorage.getItem("SelecionadoCodigoCliente");
   
       this.PedidoForm.patchValue({Cliente:SelecionadoCliente});
       this.PedidoForm.patchValue({codigoCliente:SelecionadoCodigoCliente});
    });
     
  
};

}








  