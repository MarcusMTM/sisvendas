import { Tamanho } from '../tamanho/tamanho';
import { Colecao } from '../colecao/colecao';

export interface ProdutosSelecionado {
    
    codigoProduto: number;
	//nome: string;
	descricao: string ;
	codigoTamanho :number;
	codigoColecao :number;
    valor :number  ;
	status:boolean;
	// constructor(codigoTamanho:Tamanho,descricao:string,codigoColecao:Colecao,descricaocolecao:string){
	// 	codigoTamanho = codigoTamanho;
	// 	codigoColecao=codigoColecao;
	// 	descricao=descricao;
	// 	descricaocolecao=descricao;
	//  }
     desconto :number ;
     valorValorTotalItem :number;
     quantidadeItem:number;
}
