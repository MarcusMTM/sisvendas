
import { Component, OnInit,NgZone,Input ,OnChanges,ViewChild,ElementRef  } from '@angular/core';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PedidoService } from '../../services/pedido.service';
import { PedidoitemService } from '../../services/pedidoitem.service';
import { FormadepagamentoService } from '../../services/formadepagamento.service';
import { Cliente } from '../../cliente/cliente';
import { Produtos } from '../../produto/Produtos';
import { formapagamento } from '../../pedido/formapagamento';
import { Observable } from 'rxjs';
import { ClienteService } from '../../services/cliente.service';
import { ProdutoService } from '../../services/produto.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { pedidoitem } from '../../pedidoitem/pedidoItem';
import {NgbModal, NgbActiveModal,ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { DatePipe ,CurrencyPipe} from '@angular/common';
@Component({
  selector: 'app-detail-pedido',
  templateUrl: './detail-pedido.component.html',
  styleUrls: ['./detail-pedido.component.scss']
})
export class DetailPedidoComponent implements OnInit {

  inputValue = "";
  values = 0;
  valortotal=0;
  PedidoForm: FormGroup;
  pedidoItemForm: FormGroup;
  classgeneric: any = [];
  cliente$: Observable<Cliente[]>;
  produto$: Observable<Produtos[]>;
 
  formapagamento$: Observable<formapagamento[]>;
 
  myControl = new FormControl();
  options: Cliente[];
  optionsformapagamentos: formapagamento[];

  filteredOptions: Observable<Cliente[]>;
  filteredOptionsProdutos: Observable<Produtos[]>;
  filteredOptionsformapagamento:Observable<formapagamento[]>;
  ClientesSubscription: Subscription;
  ProdutosSubscription: Subscription;
  formapagamentosSubscription: Subscription;
  public pedidoitem:pedidoitem;
  
  public pedidoItemDetails;
   submitted = false;
  rows = [];
  rowspI = [];
  columns = []
  selected = [];
  constructor(

    public fb: FormBuilder,
    private ngZone: NgZone,
    private router: Router,
    public pedidoService: PedidoService,
    public clienteService: ClienteService,
    public produtoService:ProdutoService,
    public PedidoitemService:PedidoitemService,
    public FormadepagamentoService:FormadepagamentoService,
    private modalService: NgbModal
   
  ) { 
    this.myControl = new FormControl();
    this.columns = [
      {
        prop: 'selected',
        name: '',
        sortable: false,
        canAutoResize: false,
        draggable: false,
        resizable: false,
        headerCheckboxable: true,
        checkboxable: true,
        width: 30
      },
      { prop: 'descricao' },
      { prop: 'valor' },

    ]
  }
  onSelect(row) {
    console.log(row)
  }
  ngOnInit() {
    let pipe = new DatePipe('en-US');
    let moeda= new CurrencyPipe('pt-BR',);
   this.values=0
    this.addIssue()
    this.getcliente()
    this.getProdutos()
    this.getFormapagamento()
  
  
    console.log('Issue editcodigoPedido!')
      this.addIssue()
      let empid = localStorage.getItem('editcodigoPedido');  
      if (+empid > 0) {  
        this.pedidoService.getPedidosResumo(+empid).subscribe(data => {   
          console.log('pedido')
          console.log(data)
          //{dataPedido:pipe.transform(data.dataPedido, 'dd/MM/yyyy')
        
          this.PedidoForm.patchValue(
            {
             dataPedido:pipe.transform(data.dataPedido, 'dd/MM/yyyy'),
             codigoCliente:data.cliente.nome,             
             valor:data.valor,
             status:data.status,
             formapagamentoCodigo:data.formapagamento.descricao
           });    

        });  
        this.PedidoitemService.getPedidosId(+empid).subscribe(data => { 
          console.log('pedido items') 
          console.log(data)
         
         this.rowspI=data;
      
        })

      } 

  }

 
    
    
    onKey(event: any) { // without type info
      
      let tempvalor=0;
      this.values=0;
      let cont=this.selected.length
      this.values = event.target.value;
      console.log(' this.values'+ this.values)
      this.selected.forEach(novoproduto => {
     
        this.valortotal+=novoproduto.valor*this.values;
        
      });
    for (let index = 0; index < this.selected.length; index++) {
     this.selected[index];
   
      
    }     
      console.log('valor to '+this.valortotal)
    }
 

  
  ngOnDestroy() {
    //this.ClientesSubscription.unsubscribe();
    //this.formapagamentosSubscription.unsubscribe();
}


  addIssue() {
    this.PedidoForm = this.fb.group({
    
      dataPedido: [{value: '', disabled: true},Validators.required],
      formapagamentoCodigo: [{value: '', disabled: true},Validators.required],
      codigoCliente: [{value: '', disabled: true},Validators.required],
      valor: [{value: '', disabled: true},Validators.required],
      status:[{value: '', disabled: true}],
      codigoProduto:[{value: '', disabled: true},Validators.required],
      codigoPedido: 0,
      quantidadeItem:[{value: '', disabled: true},Validators.required],
      valoritem:[{value: '', disabled: true}],
      codigoPedidoitem:[{value:'',disabled:true}],
      //statusITEM:true
    })

     
    
}

submitForm() {

  this.modalService.dismissAll();
  
  this.ngZone.run(() => this.router.navigateByUrl('/ListPedidoComponent'))
  
}


getcliente(){
   
  this.cliente$= this.clienteService.getClienteList();

 }

 getFormapagamento(){
   
  this.formapagamento$= this.FormadepagamentoService.getformapagamentoList();
}

 getProdutos(){
   
  this.ProdutosSubscription= this.produtoService.getProdutosList().subscribe((produtos: Produtos[]) => {

    this.classgeneric = produtos;
    //this.rows = produtos;
    this.filteredOptionsProdutos = this.PedidoForm.valueChanges
        .pipe(
        startWith(''),
        map(classgeneric => classgeneric ? this._filter(classgeneric) : this.classgeneric.slice())
        );
});    

 }

  _filter(value: string): Cliente[] {
  const filterValue = value.toLowerCase();
  return this.options.filter(x =>
    x.nome.toUpperCase().indexOf(filterValue.toUpperCase()) !== -1);
  
}

add() {
  this.selected.push(this.rows[1], this.rows[3]);
  
  }
 


update() {
  this.selected = [this.rows[1], this.rows[3]];
}

remove() {
  this.selected = [];
  this.valortotal=0;
}

}
