import { Component, OnInit, NgZone,ViewChild  } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Pedido } from '../pedido';
import { Cliente } from '../../cliente/cliente';
import { Produtos } from '../../produto/Produtos';
import { formapagamento } from '../../pedido/formapagamento';
import { PedidoService } from '../../services/pedido.service';
import { Observable,Subject, merge } from 'rxjs';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import { FormadepagamentoService } from '../../services/formadepagamento.service';
import { ClienteService } from '../../services/cliente.service';
import { ProdutoService } from '../../services/produto.service';
import { PedidoitemService } from '../../services/pedidoitem.service';
import {FormControl} from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {map, startWith, debounceTime, distinctUntilChanged, switchMap, catchError,filter} from 'rxjs/operators';
import { DatePipe,CurrencyPipe } from '@angular/common';
import {NgbTypeahead,NgbTypeaheadSelectItemEvent} from '@ng-bootstrap/ng-bootstrap';
import { StringUtils } from 'src/app/utils/string-utils';

import { NgxSpinnerService } from "ngx-spinner";
import {ListaclienteComponent} from '../listacliente/listacliente.component'
import { ProdutosSelecionado } from '../ProdutoSelecionado';
@Component({
  selector: 'app-edit-pedido',
  templateUrl: './edit-pedido.component.html',
  styleUrls: ['./edit-pedido.component.scss']
})
export class EditPedidoComponent implements OnInit {
  myControl = new FormControl();
  constructor( public fb: FormBuilder,
    private ngZone: NgZone,
    private router: Router,
    
    public pedidoService: PedidoService,
    public clienteService: ClienteService,
    public produtoService:ProdutoService,
    public PedidoitemService:PedidoitemService,
    public FormadepagamentoService:FormadepagamentoService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService
   
    ) {
      this.myControl = new FormControl();
      this.columns = [
        {
          prop: 'selected',
          name: '',
          sortable: false,
          canAutoResize: false,
          draggable: false,
          resizable: false,
          headerCheckboxable: true,
          checkboxable: true,
          width: 30
        },
        { prop: 'descricao' },
        { prop: 'valor' },
  
      ]
     }
     model: Cliente;
    PedidoForm: FormGroup;
    pedidoItemForm: FormGroup;
    submitted = false;
    formapagamento$: Observable<formapagamento[]>;
    cliente$: Observable<Cliente[]>;
    ClientesSubscription: Subscription;
    ProdutosSubscription: Subscription;
    formapagamentosSubscription: Subscription;
    classgeneric: any = [];
    rows = [];
    filteredOptions: Observable<Cliente[]>;
    filteredOptionsProdutos: Observable<Produtos[]>;
    filteredOptionsformapagamento:Observable<formapagamento[]>;
    options: Cliente[];
    selected = [];
    selecionado=[];
    selecionado2=[];
    selecionadoFinal=[];
    columns = []
    inputValue = "";
    values = 0;
    valortotal=0;
    quantidade = 0;
  ngOnInit() {
    let pipe = new DatePipe('en-US');
    const num = 12345;
    
    //this.currencyPipe.transform('99999999999999.9999', 'USD', undefined, '1.4-4')
    let num2 = this.getFormattedPrice(num);
   
    this.addIssue()
    this.getcliente()
    this.getProdutos()
    this.getFormapagamento()
  
    this.ClientesSubscription = this.clienteService.getClienteList().subscribe((clientes: Cliente[]) => {

    this.options = clientes;
    this.filteredOptions = this.myControl.valueChanges
        .pipe(
        startWith(''),
        map(options => options ? this._filter(options) : this.options.slice())
          );
  }); 
    let empid = localStorage.getItem('editcodigoPedido');  
    if (+empid > 0) {  
      this.pedidoService.getPedidosResumo(+empid).subscribe(data => {  
   
       this.PedidoForm.patchValue(
        {
         codigoPedido:data.codigoPedido,
         dataPedido:pipe.transform(data.dataPedido,'yyyy-MM-dd'),
         codigoCliente:data.cliente.codigoCliente,
         Cliente:data.cliente.codigoCliente+":"+data.cliente.nome,       
         valor: this.getFormattedPrice(data.valor),//money.transform(data.valor,'1.2-2'),
         formapagamentoCodigo:data.formapagamentoCodigo,
         status:data.status//,
       
       });  
      
      this.pedidoService.getPedidosProdutoResumo(+empid).subscribe(data => {
            this.selected=data          
            this.calcularvalorfinal(this.selected); 
        })
 
      })  
     
    }
    
  }
  getFormattedPrice(price: number) {
    return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(price);
}
  onSelect(row) {
    console.log(row)
  }
  addIssue() {
    this.PedidoForm = this.fb.group({
    
      dataPedido: ['',Validators.required],
      formapagamentoCodigo: ['',Validators.required],
      codigoCliente:[''],
      Cliente: ['',Validators.required],
      valor: [''],
      status:true,
      //dataPedidoITEM:[''],
      codigoProduto:[''],
      codigoPedido: 0,
      quantidadeItem: ['',Validators.required],
      valoritem:[''],
      ValorTotalItem:[''],
      Desconto:['']
    })

         
}
removeItem(event: any){
  this.selecionadoFinal =this.selecionadoFinal.filter(item => item.codigoProduto !== event );
  console.log(this.selecionadoFinal)
  let contSelecionado=this.selecionadoFinal.length
  if(contSelecionado>1){
    this.calcularvalorfinal(this.selecionadoFinal); 
      }
  else{
        this.selected=this.selected.filter(x =>
          x.codigoProduto!==(event)); 
          console.log(this.selected)
        this.calcularvalorfinal(this.selected); 
      }

}
onKey(event: any,codigoproduto:number) { // without type info

  this.quantidade=0;
  this.valortotal=0;
  let descricao;
  let exampleArray : any=[];
  let listatemp : any=[];
  let cont=this.selecionado2.length

  this.quantidade = parseInt(event.target.value);
  let newArray : any=[];
  this.selected.forEach(function(item) {
    newArray= {
      quantidadeItem:item.quantidadeItem,
      codigoProduto:item.codigoProduto,
      valor: item.valor,
      descricao:item.descricao 
     
    };
    listatemp.push(newArray);
  });

  if (cont==0){
  this.selecionado= this.selected.filter(x =>
    x.codigoProduto==(codigoproduto)   
    ); 
    this.selecionado.forEach(novoproduto => {
      descricao=novoproduto.descricao
      this.valortotal+=novoproduto.valor*this.quantidade;
      
    });  
  }
  else{

    this.selecionado2= this.selected.filter(x =>
      x.codigoProduto==(codigoproduto)   
      ); 
      this.selecionado2.forEach(novoproduto => {
        descricao=novoproduto.descricao
        this.valortotal+=novoproduto.valor*this.quantidade;
        
      });  
    
  }
  
  exampleArray= {
          quantidadeItem:this.quantidade ,
          codigoProduto:codigoproduto,
          valor: this.valortotal,
          descricao:descricao        
         
        } ;
  this.selecionado2 =this.selected.filter(item => item.codigoProduto !== codigoproduto );
  this.selecionado2.push(exampleArray)
  this.selecionadoFinal =this.selecionadoFinal.filter(item => item.codigoProduto !== codigoproduto );
  this.selecionadoFinal.push(exampleArray);
  let contSelecionado=this.selecionadoFinal.length
  if(contSelecionado>1){
    this.calcularvalorfinal(this.selecionadoFinal); 
      }
      else{
        this.calcularvalorfinal(this.selecionado2); 
      }

}
calcularvalorfinal(lista:any){
  let valorfinal=0
  let ValorTotalItem=0  
  lista.forEach(novoproduto => {
    ValorTotalItem=novoproduto.valor
    valorfinal+=novoproduto.valor;    
  });
   this.PedidoForm.patchValue(
    {
      ValorTotalItem:ValorTotalItem,
      valor: valorfinal
    }
  )
}
AddItems(){
  this.selecionado=this.selected  
  let valorfinal=0
  let ValorTotalItem=0

  this.selecionado2.forEach(novoproduto => {
    ValorTotalItem=novoproduto.valor
    valorfinal+=novoproduto.valor;
    
  });
   this.PedidoForm.patchValue(
    {
      ValorTotalItem:ValorTotalItem,
      valor: valorfinal
    }
  )

}
cadastrar(){
  this.submitted = true;
  //if (this.PedidoForm.valid) {
    this.spinner.show();
    this.selected.forEach(novoproduto => {   
     
      this.PedidoForm.patchValue({codigoProduto: novoproduto.codigoProduto, valoritem: novoproduto.valor,quantidadeItem:novoproduto.quantidadeItem });
      this.pedidoService.AlterarPedidos(this.PedidoForm.value).subscribe(res=>{

        console.log('cadastrado')
      } 
                
        
      )
     
    });

    //this.pedidoService.AlterarPedidos(this.PedidoForm.value).subscribe(res => {
      //this.modalService.dismissAll();
      setTimeout(() => {
        /** spinner ends after 5 seconds */
      this.spinner.hide();     
      this.PedidoForm.reset();
      this.router.navigate(['ListPedidoComponent']);
      this.modalService.dismissAll();
      //this.router.navigate(['ListPedidoComponent']);
     // this.router.navigateByUrl('/ListPedidoComponent')
      }, 5000);
     
        //this.ngZone.run(() => this.router.navigateByUrl('/ListPedidoComponent'))
   // });
  //}
    // else{
    //   return;
    // }
}
submitForm() {
  this.submitted = true;
 // if (this.PedidoForm.valid) {
    this.spinner.show();
    
    this.pedidoService.AlterarPedidos(this.PedidoForm.value).subscribe(res => {
      this.modalService.dismissAll();
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
     
      this.PedidoForm.reset();
     
      }, 1000);
        //this.ngZone.run(() => this.router.navigateByUrl('/ListPedidoComponent'))
    });
  //}
    // else{
    //   return;
    // }
  
}

getcliente(){
   
  this.cliente$= this.clienteService.getClienteList();

 }

 getFormapagamento(){
   
  this.formapagamento$= this.FormadepagamentoService.getformapagamentoList();
}
_filter(value: string): Cliente[] {
  const filterValue = value.toLowerCase();
  return this.options.filter(x =>
    x.nome.toUpperCase().indexOf(filterValue.toUpperCase()) !== -1);
  
}
 getProdutos(){
   
  this.ProdutosSubscription= this.produtoService.getProdutosList().subscribe((produtos: Produtos[]) => {

    this.classgeneric = produtos;
    this.rows = produtos;
    this.filteredOptionsProdutos = this.PedidoForm.valueChanges
        .pipe(
        startWith(''),
        map(classgeneric => classgeneric ? this._filter(classgeneric) : this.classgeneric.slice())
        );
});    

 }
 remove() {
  this.selected = [];
  this.valortotal=0;
}
ProdutoFilter(event) {
  const val = event.target.value.toLowerCase();
 
 
  const temp = this.rows.filter(x => 
    
    x.descricao.toString().toLowerCase().includes(val.toString().toLowerCase())
  
 
  )
  if(val && val.trim() != ''){
    this.rows = temp;
  }
  else{
    this.getProdutos()
    
  }
}
search = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term === '' ? []
      : this.options.filter(v => v.nome.toLowerCase().indexOf(term.toLowerCase()) > -1 ||
      v.codigoCliente.toString().indexOf(term) > -1
      ).slice(0, 10)
      
      )
      
  )
  formatter = (result: any) => result.codigoCliente+":"+ result.nome.toLowerCase();
  

searchcliente = (text$: Observable<string>) =>
text$.pipe(
  debounceTime(200),
  map(term => term === '' ? []
    : this.options.filter(v => v.nome.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    
)


formatter3 = (result: Cliente) => result.codigoCliente+":"+ result.nome.toLowerCase();
public model3: Cliente;

//teste = (result: Cliente) => result.codigoCliente+":"+ result.nome.toLowerCase();

teste = (result:Cliente) => result.nome;
search3 = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term === '' ? []
      : this.options.filter(v => v.nome.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  )
  setModel(e: NgbTypeaheadSelectItemEvent, fubi: any) {
    this.model = e.item.name;
  }


voltar(): void {
  this.modalService.dismissAll();
  localStorage.removeItem("editcodigoPedido"); 

  this.router.navigate(['ListPedidoComponent']);

};
View() { 
  
  const modalRef = this.modalService.open(ListaclienteComponent, { size: 'lg' , centered: true  });

    modalRef.result.then((result) => {
      console.log(result);
      let SelecionadoCliente = localStorage.getItem('SelecionadoCliente');
      let SelecionadoCodigoCliente= localStorage.getItem("SelecionadoCodigoCliente");
      console.log('SelecionadoCliente') 
      
      this.PedidoForm.patchValue({Cliente:SelecionadoCliente});
      this.PedidoForm.patchValue({codigoCliente:SelecionadoCodigoCliente});
    }).catch((error) => {
      console.log('error');
      let SelecionadoCliente = localStorage.getItem('SelecionadoCliente');
      let SelecionadoCodigoCliente= localStorage.getItem("SelecionadoCodigoCliente");
   
       this.PedidoForm.patchValue({Cliente:SelecionadoCliente});
       this.PedidoForm.patchValue({codigoCliente:SelecionadoCodigoCliente});
    });
     
  
};
}
