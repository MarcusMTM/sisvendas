import { Component, OnInit,Inject,NgZone } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Cliente } from '../../cliente/cliente';
import { ClienteService } from '../../services/cliente.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import{CreateClienteComponent} from '../../cliente/create-cliente/create-cliente.component'

import {debounceTime, map} from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-listacliente',
  templateUrl: './listacliente.component.html',
  styleUrls: ['./listacliente.component.scss']
})
export class ListaclienteComponent implements OnInit {
  public Clientes: Cliente[];
  public httpteste: HttpClient;
  public url: string;
  selected = [];
  columns = []
  rows : any;
  show: boolean = false;
  constructor(http: HttpClient,public ClienteServices: ClienteService,   private ngZone: NgZone,
    private router: Router,  public fb: FormBuilder,
    private modalService: NgbModal,private activeModal: NgbActiveModal) {
   // var teste= this.UsuarioservicesService.getUsuariologin();
   //  console.log(teste); this.getdata()
   this.getdata()
   this.columns = [
    {
      prop: 'selected',
      name: '',
      sortable: false,
      canAutoResize: false,
      draggable: false,
      resizable: false,
      headerCheckboxable: true,
      checkboxable: true,
      width: 30
    },
    { prop: 'nome' },
    { prop: 'dataNascimento' },
    { prop: 'instagram' },
    { prop: 'whatsapp' },
    

  ]

}

  ngOnInit() {
    this.show = false;
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
   
   
    const temp = this.rows.filter(x => 
      
      x.nome.toString().toLowerCase().includes(val.toString().toLowerCase())
   
    )
    if(val && val.trim() != ''){
      this.rows = temp;
    }
    else{
      this.getdata()
      
    }
  }
   
 
  
 
  onSelect(row) {
    console.log(row)
   // this.modalService.dismissAll(); 
    console.log('novoproduto.nome');
    this.selected.forEach(novoproduto => {
        console.log(novoproduto.nome);
        localStorage.setItem("SelecionadoCliente",novoproduto.codigoCliente+":"+novoproduto.nome.toString());
        localStorage.setItem("SelecionadoCodigoCliente",novoproduto.codigoCliente);
    })
    this.activeModal.close();
    //this.modalService.dismissAll();
  }
  onKey(event: any) { // without type info
      
    let tempvalor=0;
  
    let cont=this.selected.length
    let count= this.selected.length
  
    const firstElement = this.selected.shift()
  }
  getdata(){
    return( this.ClienteServices.getClienteList().subscribe(result => {
     
      this.Clientes = result;
      this.rows = result;
    }, error => console.error(error)))
  }

  
  search = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    map(term => term === '' ? []
      : this.rows.filter(v => v.nome.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
      
  )

formatter = (x: {nome: string}) => x.nome;

voltar() : void {
  this.modalService.dismissAll(); 
  localStorage.removeItem("SelecionadoCliente");
}

add() : void {
  this.modalService.dismissAll(); 
   
  //console.log('views'+pedido.codigoPedido)
  
  this.router.navigate(['CreateCliente']);
 
}
}
