import { Component, OnInit,Inject,NgZone } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Pedido } from '../pedido';
import { PedidoService } from '../../services/pedido.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import {NgbModal, ModalDismissReasons,NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {DetailPedidoComponent} from '../detail-pedido/detail-pedido.component';
import {EditPedidoComponent} from '../edit-pedido/edit-pedido.component'
import {PedidoitemGestaoComponent} from '../pedidoitem-gestao/pedidoitem-gestao.component'
import {debounceTime, map} from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-list-pedido',
  templateUrl: './list-pedido.component.html',
  styleUrls: ['./list-pedido.component.scss']
})
export class ListPedidoComponent implements OnInit {

  public Pedidos: Pedido[];
  public httpteste: HttpClient;
  public url: string;
 
  Pedido$: Observable<Pedido[]>;
  show: boolean = false;
  constructor(http: HttpClient, public PedidoServices: PedidoService, private ngZone: NgZone,
    private router: Router,  public fb: FormBuilder,
    private modalService: NgbModal,
    
    public toastrService: ToastrService) {
    
    this.Pedido$= this.PedidoServices.getPedidosList();
    this.PedidoServices.getClientelogin();
    console.log(this.Pedido$)

}
columnDefs = [
  {field: 'dataPedido' },
  {field: 'formapagamentoCodigo' },
  {field: 'codigoCliente'},
  {field: 'valor'},
  {field: 'status'},
  
];
rows : any;
columns = [
  { prop: 'name' },
  { name: 'Gender' },
  { name: 'Company' }
];
rowData: any;

ngAfterViewInit() {
  console.log("---ngAfterViewInit() Demo---");
  this.getdata()
}

  ngOnInit() {
    this.show = false;
    this.getdata()
  }

  
  gestao(pedido: Pedido): void {
    localStorage.removeItem("editcodigoPedido");
    localStorage.setItem("editcodigoPedido", pedido.codigoPedido.toString());
    const modalRef = this.modalService.open(PedidoitemGestaoComponent,  { size: 'lg', backdrop: 'static' });
  
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
  };
  onUpdate(pedido: Pedido) {  
    localStorage.removeItem("editcodigoPedido");
    localStorage.setItem("editcodigoPedido", pedido.codigoPedido.toString());
    // this.ClienteServices.updateUser(this.ColecaoForm.value).subscribe(data => {  
    //   this.router.navigate(['ListColecao']);  
    // },  
    //   error => {  
    //     alert(error);  
    //   });  
  }  
  edit(pedido: Pedido): void {
    console.log('edit'+pedido.codigoPedido)
    localStorage.removeItem("editcodigoPedido");
    localStorage.setItem("editcodigoPedido", pedido.codigoPedido.toString());
    const modalRef = this.modalService.open(EditPedidoComponent,  { size: 'lg', backdrop: 'static' });
  
    modalRef.result.then((result) => {
      console.log(result);
      this.getdata()
    }).catch((error) => {
      console.log(error);
      this.getdata()
    });
   // this.router.navigate(['EditPedido']);
    //this.router.navigate(['ListColecao']);  
  };
  View(pedido: Pedido): void {
    console.log('views'+pedido.codigoPedido)
    localStorage.removeItem("editcodigoPedido");
    localStorage.setItem("editcodigoPedido", pedido.codigoPedido.toString());
    const modalRef = this.modalService.open(DetailPedidoComponent, { size: 'lg', backdrop: 'static' });
  
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
   // this.router.navigate(['DetalhePedido']);
    //this.router.navigate(['ListColecao']);  
  };
  getdata(){
    return( this.PedidoServices.getPedidosList().subscribe(result => {     
      this.Pedidos = result;
      this.rowData=result;
      this.rows=result;
      
      console.log(this.rows)
    }, error => {
    
    console.error(error.status)
    })
    )
  }
  
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
   
   
    const temp = this.rows.filter(x => 
      
      x.codigoPedido.toString().toLowerCase().includes(val.toString().toLowerCase())
      ||
      x.codigoCliente.toString().toLowerCase().includes(val.toString().toLowerCase())
      ||
      x.cliente.nome.toString().toLowerCase().includes(val.toString().toLowerCase())
   
    )
    console.log('temp')
    console.log(temp)
    if(val && val.trim() != ''){
      this.rows = temp;
    }
    else{
      this.getdata()
      
    }
  }
  search = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    map(term => term === '' ? []
      : this.rows.filter(v => v.codigoPedido.toString().indexOf(term.toLowerCase()) > -1 ||
      v.codigoCliente.toString().indexOf(term.toLowerCase()) > -1
      ).slice(0, 10)
      
      )
      
  )

formatter = (x: {pedido: Pedido}) => x.pedido;

}

